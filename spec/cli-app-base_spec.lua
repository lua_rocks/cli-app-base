-- 30-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("cli-app-base");

local resdir = './spec/resources/'
local get_resource = M.build_fn_get_resource(resdir)
local get_res_path = M.build_fn_get_resource_path(resdir)

describe("base", function()
  it("tbl_index_of", function()
    local f = M.tbl_index_of
    assert.same(3, f({ 'a', 'b', 'c' }, 'c'))
    assert.same(1, f({ 'a', 'b', 'c' }, 'a'))
    assert.same(-1, f({ 'a', 'b', 'c' }, 'A'))
    assert.same(2, f({ 'a', 'b', 'c' }, 'b'))
    assert.same(-1, f({ 'a', 'b', 'c', 4 }, nil))
    assert.same(4, f({ 'a', 'b', 'c', 8 }, 8))
  end)

  it("tbl_list_to_map", function()
    local f = M.tbl_list_to_map
    assert.same({ a = 1, b = 1, c = 1, [8] = 1 }, f({ 'a', 'b', 'c', 8 }, 1))
  end)

  it("tbl_map_to_list", function()
    local f = M.tbl_map_to_list
    local map = { a = 1, b = 2, c = 3, [8] = 4 }
    local exp = { { 'a', 1 }, { 8, 4 }, { 'c', 3 }, { 'b', 2 } }
    assert.same(exp, f(map))
  end)

  it("tbl_join_lists_to_new", function()
    local f = M.tbl_join_lists_to_new
    local t1 = { '1', '2', '3' }
    local t2 = { '4', '5', '6' }
    assert.same({ '1', '2', '3', '4', '5', '6' }, f(t1, t2))
  end)

  it("tbl_merge_maps_to_new", function()
    local f = M.tbl_merge_maps_to_new
    local m1 = { a = 4, b = 5, c = 8 }
    local m2 = { a = 44, c = 9, d = 12 }

    local res = f(m1, m2)
    assert.same({ a = 44, b = 5, c = 9, d = 12 }, res)
    assert.are_not_equal(m1, res) -- not a same instance!
    assert.are_not_equal(m2, res) -- not a same instance!

    -- not changed
    assert.same({ a = 4, b = 5, c = 8 }, m1)
    assert.same({ a = 44, c = 9, d = 12 }, m2)
  end)

  it("tbl_add_list all", function()
    local f = M.tbl_add_list
    local dst = { '1', '2', '3' }
    local src = { '4', '5', '6' }
    local res = M.tbl_add_list(dst, src)

    assert.same({ '1', '2', '3', '4', '5', '6' }, res)
    assert.equal(res, dst) -- same instance
  end)

  it("tbl_add_list all2", function()
    local f = M.tbl_add_list
    local dst = { '1', '2', '3' }
    local src = { '4', '5', '6' }
    local res = M.tbl_add_list(dst, src, 1, #src)

    assert.same({ '1', '2', '3', '4', '5', '6' }, res)
    assert.equal(res, dst) -- same instance
  end)

  it("tbl_add_list part", function()
    local f = M.tbl_add_list
    local dst = { '1', '2', '3' }
    local src = { '4', '5', '6', '7', '8' }
    local res = M.tbl_add_list(dst, src, 2, 4)

    assert.same({ '1', '2', '3', '5', '6', '7' }, res)
    assert.equal(res, dst) -- same instance
  end)

  it("tbl_add_list part reverse", function()
    local f = M.tbl_add_list
    local dst = { '1', '2', '3' }
    local src = { '4', '5', '6', '7', '8' }
    local res = M.tbl_add_list(dst, src, 4, 2)

    assert.same({ '1', '2', '3', '7', '6', '5' }, res)
  end)

  it("tbl_add_list all reverse", function()
    local f = M.tbl_add_list
    local dst = { '1', '2', '3' }
    local src = { '4', '5', '6', '7', '8' }
    local res = M.tbl_add_list(dst, src, #src, 1)

    local exp = { '1', '2', '3', '8', '7', '6', '5', '4' }
    assert.same(exp, res)
  end)

  it("tbl_compare_map_by_keys", function()
    local f = M.tbl_compare_map_by_keys

    local exp = { {}, {} }
    assert.same(exp, { f({ a = 1, b = 2, c = 3 }, { a = 2, b = 3, c = 4 }) })

    local exp2 = { { 'b' }, {} }
    assert.same(exp2, { f({ a = 1, c = 3 }, { a = 2, b = 3, c = 4 }) })

    local exp3 = { {}, { 'c' } }
    assert.same(exp3, { f({ a = 1, b = 2, c = 3 }, { a = 2, b = 3 }) })
  end)

  it("tbl_pop_elm table after remove", function()
    local f = function(t, i)
      M.tbl_pop_elm(t, i)
      return t
    end
    assert.same({ 'a', 'b', 'c', 8 }, f({ 'a', 'b', 'c', 8 }, 0))
    assert.same({ 'a', 'b', 'c', 8 }, f({ 'a', 'b', 'c', 8 }, -1))
    assert.same({ 'a', 'b', 'c', 8 }, f({ 'a', 'b', 'c', 8 }, 99))
    assert.same({ 'a', 'b', 'c' }, f({ 'a', 'b', 'c', 8 }, 4))
    assert.same({ 8, 'b', 'c' }, f({ 'a', 'b', 'c', 8 }, 1))
    assert.same({ 'a', 8, 'c' }, f({ 'a', 'b', 'c', 8 }, 2))
    assert.same({ 'a', 'b', 8 }, f({ 'a', 'b', 'c', 8 }, 3))
  end)

  it("tbl_pop_elm ret val", function()
    local f = M.tbl_pop_elm
    assert.is_nil(f({ 'a', 'b', 'c', 8 }, 0))
    assert.is_nil(f({ 'a', 'b', 'c', 8 }, -1))
    assert.is_nil(f({ 'a', 'b', 'c', 8 }, 99))
    assert.same(8, f({ 'a', 'b', 'c', 8 }, 4))
    assert.same('a', f({ 'a', 'b', 'c', 8 }, 1))
    assert.same('b', f({ 'a', 'b', 'c', 8 }, 2))
    assert.same('c', f({ 'a', 'b', 'c', 8 }, 3))
    assert.same(8, f({ 'a', 'b', 'c', 8 }, 4))
  end)

  it("tbl_key_count", function()
    local f = M.tbl_key_count
    assert.same(3, f({ k = 1, b = 2, 'abc' }))
    assert.same(3, f({ 42, 33, 88 }))
    assert.same(0, f({}))
    assert.same(1, f({ k = 42 }))
  end)

  it("tbl_format_as_list", function()
    local f = M.tbl_format_as_list
    local t = { a = 1, b = 2, c = 3 }
    assert.same("{\n  a = 1,\n  b = 2,\n  c = 3\n}", f(t))

    local order = { 'a', 'b', 'c' }
    assert.same("a: 1\nb: 2\nc: 3\n", f(t, order))

    local order2 = { 'a', 'b' }
    assert.same("a: 1\nb: 2\nc: 3\n", f(t, order2))

    local opts = { ignore_keys = { b = 1 } }
    assert.same("a: 1\nc: 3\n", f(t, order2, opts))
  end)

  it("extract_path", function()
    local f = M.extract_path
    assert.same('/tmp/dir/', f('/tmp/dir/file'))
    assert.same('/tmp/dir/', f('/tmp/dir/'))
    assert.same('/tmp/', f('/tmp/'))
    assert.same('/', f('/tmp'))
    assert.same('/', f('/'))
    assert.is_nil(f('tmp'))
  end)

  it("is_filename", function()
    local f = M.is_filename
    assert.same(false, f('.'))
    assert.same(false, f('..'))
    assert.same(false, f('/.'))
    assert.same(false, f('/..'))
    assert.same(true, f('/path/file'))
    assert.same(false, f('/path/file*'))
    assert.same(false, f('/path/file*'))
    assert.same(false, f('/path/file; rm -rf *'))
  end)

  it("join_path", function()
    local f = M.join_path
    assert.same("", f())
    assert.same("", f(nil))
    assert.same("/", f('/', '/'))
    assert.same("/tmp", f('/', '/tmp'))
    assert.same("/tmp/tmp", f('/tmp', '/tmp'))
    assert.same("/tmp/tmp", f('/tmp/', '/tmp'))
    assert.same("/tmp//tmp", f('/tmp//', '/tmp'))
    assert.same("/tmp", f('/', 'tmp'))
    assert.same("/tmp/file", f('/tmp', 'file'))
    assert.same("/tmp/file", f('/tmp', '/file'))
    assert.same("/tmp/file", f('/tmp/', '/file'))
    assert.same("/tmp/file", f('/', 'tmp/', 'file'))
  end)

  it("ensure_dirname", function()
    local f = M.ensure_dirname
    assert.same('./', f(''))
    assert.same('/home/user/', f('/home/user'))
    assert.same('/home/user/', f('/home/user/'))
  end)

  it("extract_extension", function()
    local f = M.extract_extension
    assert.same("", f("/tmp/path/to/File"))
    assert.same("md", f("/tmp/path/to/readme.md"))
    assert.same("txt", f("/tmp/path/to/readme.txt"))
    assert.same("txt", f("/tmp/path/to/readme.1.txt"))
    assert.same("txt", f("readme.1.txt"))
    assert.same("txt", f("readme/.txt"))
    assert.same("a", f("readme/1.a"))
    assert.same("git", f(".git"))
    assert.same("git", f("/.git"))
    assert.same("e", f("/.git.e"))
    assert.same("ext", f("/.git.e/file.ext"))
    assert.same("", f("/home/user/.dotfiles/tools/tools/tool"))
    assert.same('sh', f("/home/user/.dotfiles/tools/tools/tool.sh"))
    assert.same('', f("ex"))
    assert.same('ex', f(".ex"))
  end)

  it("get_parent_dirpath", function()
    local f = M.get_parent_dirpath
    assert.is_nil(f("/"))
    assert.is_nil(f("//"))
    assert.is_nil(f("///"))
    assert.same("//", f("//a/"))
    assert.same("/", f("/dir"))
    assert.same("/", f("/dir/"))
    assert.same("/dir/", f("/dir/sub"))
    assert.same("/dir/", f("/dir/sub/"))
    assert.same("/dir/", f("/dir/sub//"))
    assert.same("/dir/sub/", f("/dir/sub/a"))
    assert.same("/dir/sub/", f("/dir/sub/a/", '/'))
    assert.same('\\dir\\sub\\', f("\\dir\\sub\\a\\", '\\'))
    assert.same('.dir.sub.', f(".dir.sub.a.", '.'))
  end)

  it("build_path", function()
    local f = M.build_path
    assert.same("/root", f("/home/dev/pro/", "/root"))
    assert.same('/home/dev/', f("/home/dev/pro/", ".."))
    assert.same('/', f("/home/", ".."))
    assert.same('/', f("/home", ".."))
    assert.same('/', f("/", ".."))
    assert.same("/home/dev/pro/api", f("/home/dev/pro/", "./api"))
    assert.same("/home/dev/api", f("/home/dev/pro/", "../api"))
    assert.same("/home/api", f("/home/dev/pro/", "../../api"))
    assert.same("/api", f("/home/dev/", "../../api"))
    assert.same("/api", f("/home/", "../../api"))
    assert.same("/home/", f("/home/", nil))
    assert.same("/", f("/home/", '/'))
    assert.same("/home/", f("/home/", './'))
    assert.same("/home/", f("/home", './'))
    assert.same("/home/dev", f("/home", 'dev'))
    assert.same("//dev", f("/home", '//dev')) -- double slash issue
    assert.same("/", f("/home/", '../'))
    assert.same("/", f("/home/", '../.././'))
    assert.same("/home/dev/pro/api", f("/home/dev/pro/", "././api"))
    assert.same("/ab/cd", f("/ab/cd/ef/", '../../cd'))
    assert.same("/a/b/c", f("/a/b/c/d/e", '../../../c'))
  end)

  it("basename", function()
    local f = M.basename
    assert.same("/", f("/"))
    assert.same("file", f("/file"))
    assert.same("file", f("path/to/file"))
    assert.same("file", f("/path/to/file"))
    assert.same("/path/to/dir/", f("/path/to/dir/"))
    assert.same("", f(""))
    assert.is_nil(f(nil))
  end)

  it("tbl_deep_copy", function()
    local tbl_def = {
      key1 = 'a',
      key2 = 'b',
      key3 = { ksub1 = 'd1', ksub4 = 8 },
      key4 = { ksub01 = 'e0' },
      key5 = { 'a', 'b', 'e' },
      keyQ = 'q',
    }
    local exp = {
      key1 = 'a',
      key2 = 'b',
      key3 = { ksub4 = 8, ksub1 = 'd1' },
      key4 = { ksub01 = 'e0' },
      key5 = { 'a', 'b', 'e' },
      keyQ = 'q'
    }
    assert.same(exp, M.tbl_deep_copy(tbl_def))
  end)

  it("tbl_keys_filter_by_type", function()
    local f = M.tbl_keys_filter_by_type
    local map = { a = 1, b = true, c = 'a', d = {}, e = {} }
    assert.same({ { 'e', 'd' }, { 'a', 'c', 'b' } }, { f(map, 'table') })
  end)

  it("tbl_max_str_key_len", function()
    local f = M.tbl_max_str_key_len
    assert.same(3, f({ 'abc', 'de', 'f' }))
    assert.same(4, f({ 'abc', 'de', 'f', '1234' }))
    assert.same(5, f({ 'abc', 'de', 'f', '1234' }, 5))
  end)
end)

describe("base IO", function()
  local tbl_path = get_res_path('table.lua.1') -- M.join_path

  it("is_os_windows", function()
    assert.same(false, M.is_os_windows())
  end)

  it("read_content", function()
    local exp = "{\n  key = \"abc\",\n  list = {1, 2, 3},\n}\n\n"
    assert.same(exp, M.read_content(tbl_path))
  end)

  it("copy_file", function()
    if 0 == 0 then return end -- OFF
    local f = M.copy_file
    local src = get_res_path('table.lua.1')
    local dst = get_res_path('table.lua.1.copy')
    assert.same({ true }, { f(src, dst) })

    local exp = {
      false,
      'cannot rewrire already existed file: ./spec/resources/table.lua.1.copy'
    }
    assert.same(exp, { f(src, dst) })

    assert.same({ true }, { f(src, dst, true) }) -- can rewrite
    os.remove(dst)
  end)

  it("load_lua_table", function()
    local exp = { list = { 1, 2, 3 }, key = 'abc' }
    assert.same(exp, M.load_lua_table(tbl_path))
    assert.same(exp, M.load_lua_table('table.lua.1', resdir))
  end)

  it("save_lua_table", function()
    local t = { a = 8, b = 16, c = { d = "value" } }
    assert.same(true, M.save_lua_table(t, 'tbl-2.lua', resdir))

    local exp = { b = 16, a = 8, c = { d = 'value' } }
    assert.same(exp, M.load_lua_table('tbl-2.lua', resdir))
    os.remove(resdir .. 'tbl-2.lua')
  end)

  it("parse_text_to_lua_table", function()
    local f = M.parse_text_to_lua_table
    local lines = {
      '# comment',
      'key    value',
      'key_b  value2',
      'key_c  value3',
    }
    local exp = { key = 'value', key_b = 'value2', key_c = 'value3' }
    assert.same(exp, f(lines))
  end)

  --[[

  -- This is a way that works on both Unix and Windows
  -- without any external dependencies:

  --- Check if a file or directory exists in this path
  function exists(file)
     local ok, err, code = os.rename(file, file)
     if not ok then
        if code == 13 then -- Permission denied, but it exists
           return true
        end
     end
     return ok, err
  end

  --- Check if a directory exists in this path
  function isdir(path)
     return exists(path.."/") -- "/" works on both Unix and Windows
  end
  ]]

  it("dir_exists", function()
    assert.same(true, M.dir_exists('/tmp/'))
    -- touch /tmp/dir && file /tmp/dir # empyt   dir_exists --> false
    -- mkdir /tmp/dir && file /tmp/dir --> /tmp/dir: directory --> true
    -- assert.same(true, M.dir_exists('/tmp/dir'))
  end)

  it("read_first_lines", function()
    local f = M.read_first_lines
    local path = get_res_path('table.html')
    local exp = {
      {
        '<html>',
        '<style>',
        '  table.myTable {',
        '    border: 0px solid #FFFFFF;'
      },
      4 -- readed lines
    }
    assert.same(exp, { f(path, 4) })

    local exp2 = { { '<html>' }, 1 }
    assert.same(exp2, { f(path, 1) })

    local exp3 = {
      { '{', '  key = "abc",', '  list = {1, 2, 3},', '}', '' },
      5
    }
    assert.same(exp3, { f(get_res_path('table.lua.1'), 8) })
  end)

  it("filename_without_extension", function()
    local f = M.filename_without_extension
    assert.same('/tmp/file', f('/tmp/file.ext'))
    assert.same('/tmp/file', f('/tmp/file'))
    assert.same('/tmp/', f('/tmp/'))
    assert.same('/', f('/'))
    assert.same('', f(''))
  end)

  it("get_resource and factory", function()
    local exp = "{\n  key = \"abc\",\n  list = {1, 2, 3},\n}\n\n"
    assert.same(exp, get_resource('table.lua.1'))

    assert.match_error(function()
      get_resource('not-exists-file')
    end, 'Not Found file')
  end)

  it("build_fn_get_resource_path", function()
    local get_respath0 = M.build_fn_get_resource_path('/root/')
    assert.same('/root/filename.ext', get_respath0('filename.ext'))
    assert.same('/root/sudbir/fn.ext', get_respath0('sudbir', 'fn.ext'))
  end)

  it("vprint", function()
    local prev = io.stdout
    local i, output = 0, ''
    io.stdout = {
      ---@diagnostic disable-next-line: unused-local
      write = function(self, line)
        output = output .. tostring(line)
        i = i + 1
      end,
      ---@diagnostic disable-next-line: unused-local
      flush = function(self)
        i = i + 1
      end
    }
    local res = M.vprint(true, "a", "b", 8, 16, "c\n")
    assert.same("a b 8 16 c\n", res)
    assert.same(2, i)
    assert.same("a b 8 16 c\n", output)

    assert.same("message", M.vprint(true, "message"))
    assert.same('message 0', M.vprint(true, "message", 0))
    assert.same('message 0 2', M.vprint(true, "message", 0, 2))
    assert.same('message 0 nil 3', M.vprint(true, "message", 0, nil, 3))
    assert.same('message 0 nil true', M.vprint(true, "message", 0, nil, true))
    assert.same('message', M.vprint(true, "message", '', ''))
    assert.same('message b', M.vprint(true, "message", '', '', 'b'))

    finally(function() io.stdout = prev end)
  end)

  it("validate_input_n_output_filenames", function()
    local f = M.validate_input_n_output_filenames
    local exp = { false, 'expected string input-filename got: nil' }
    ---@diagnostic disable-next-line: missing-parameter
    assert.same(exp, { f() })


    local exp_next_free = { true, './spec/resources/table.lua.3' }
    assert.same(exp_next_free, { f(get_res_path('table.lua.1')) })

    local exp3 = { true, './spec/resources/table.lua.3' }
    assert.same(exp3, { f(get_res_path('table.lua.2')) })
  end)
end)

describe("base str", function()
  it("str_split", function()
    local f = M.str_split
    assert.same({ 'path', 'to', 'next' }, f('path.to.next', '.'))
    assert.same({ 'pa', 'th-to' }, f('pa.th-to', '.'))
    assert.same({ 'pa.th', 'to' }, f('pa.th-to', '-'))
  end)

  it("str_first_word", function()
    local f = M.str_first_word
    assert.same('something', f("something"))
    assert.same('some', f("some thing"))
    assert.same('some', f(" some thing"))
    assert.same('some.', f(" some. thing"))
  end)

  it("str_trim", function()
    local f = M.str_trim
    assert.same('ab c', f('  ab c '))
    assert.same('ab c', f('  ab c'))
    assert.same('ab c', f('ab c  '))
  end)

  it("str_check_maxlen", function()
    local f = M.str_check_maxlen
    local t = {}
    assert.same({ key = 5 }, f(t, 'key', 'value'))
    assert.same({ key = 9 }, f(t, 'key', 'new_value'))
  end)

  it("str_align_center", function()
    local f = M.str_align_center
    assert.same(' package  ', f("package", { ['package'] = 10 }))
    assert.same(' package  ', f("package", { ['package'] = 10 }, 'pkg'))
    assert.same(' * ', f("stars", { stars = 3 }, '*'))
    assert.same('  stars   ', f("stars", { stars = 10 }, '*'))
    assert.same('  stars  ', f("stars", { stars = 9 }, '*'))
  end)

  it("get_relative_path", function()
    local f = M.get_relative_path
    assert.same('subdir', f('/home/user/', '/home/user/subdir'))
    assert.same('subdir', f('/home/user', '/home/user/subdir'))
    assert.same('subdir/deep2', f('/home/user', '/home/user/subdir/deep2'))
    assert.same('/home/usersubdir', f('/home/user', '/home/usersubdir'))
  end)

  it("tbl_get_node", function()
    local f = M.tbl_get_node
    local t = { a = { b = { c = 42 } } }

    assert.same({ true, 42 }, { f(t, 'a.b.c') })
    assert.same({ true, { c = 42 } }, { f(t, 'a.b') })
    assert.same({ true, { b = { c = 42 } } }, { f(t, 'a') })
    assert.same({ false, 'no key at depth:3(X)' }, { f(t, 'a.b.X') })
    assert.same({ false, 'in depth:4(d) got:number' }, { f(t, 'a.b.c.d.e') })

    assert.same({ true, 42 }, { f(t, 'a/b/c', '/') })
    assert.same({ true, { b = { c = 42 } } }, { f(t, 'a', '/') })

    assert.same({ true, 99 }, { f({ key = 99 }, 'key', '/') })
    assert.same({ true, false }, { f({ key = false }, 'key', '/') })
  end)

  it("str_substr_count", function()
    local f = M.str_substr_count
    assert.same(1, f("/", "/"))
    assert.same(2, f("//", "/"))
    assert.same(2, f("a/b/c", "/"))
    assert.same(3, f("a/b/c/", "/"))
    assert.same(3, f("...a", "."))
    assert.same(1, f("...a", ".", 3))
    assert.same(1, f("some/sub", "/"))
  end)

  it("str_is_true", function()
    local f = M.str_is_true
    assert.same(true, f('1'))
    assert.same(true, f('true'))
    assert.same(false, f('false'))
    assert.same(true, f('t'))
    assert.same(true, f('+'))
    assert.same(false, f('-'))
    assert.same(false, f('TRUE'))
  end)

  it("numcnt_to_readable", function()
    local f = M.numcnt_to_readable
    assert.same('1.0K', f(1000))
    assert.same('1.0K', f(1000))
    assert.same('1.0K', f(1024))
    assert.same('1.0M', f(1000 * 1000))
    assert.same('1.6M', f(1024 * 1024 * 1.5))
    assert.same('1.0T', f(1000 * 1000 * 1000))
    assert.same('1.1T', f(1024 * 1024 * 1024))
    assert.same('1.9T', f(1024 * 1024 * 1024 * 1.8))
    assert.same('1.0K', f(1025))
  end)

  it("numsize_to_readable", function()
    local f = M.numsize_to_readable
    assert.same('0.1Kb', f(128))
    assert.same('0.5Kb', f(512))
    assert.same('1.0Kb', f(1000))
    assert.same('1.0Kb', f(1000))
    assert.same('1.0Kb', f(1024))
    assert.same('1.0Mb', f(1024 * 1024))
    assert.same('1.5Mb', f(1024 * 1024 * 1.5))
    assert.same('1.0Tb', f(1024 * 1024 * 1024))
    assert.same('1.8Tb', f(1024 * 1024 * 1024 * 1.8))
    assert.same('1.7Tb', f(1000 * 1000 * 1000 * 1.8))
    assert.same('1.0Kb', f(1025))
  end)
end)

describe("base subconf", function()
  it("normalize_path", function()
    local f = M.normalize_path
    local settings = {
      CONF_DIR = '/home/user/.config/app/',
      SHARE_DIR = '/home/user/.local/share/app/',
      CACHE_DIR = '/home/user/.cache/app/',
    }
    local exp = '/home/user/.config/app/smth.lua'
    assert.same(exp, f(settings, '${CONF_DIR}/smth.lua'))

    local exp2 = '/home/user/.local/share/app/smth.lua'
    assert.same(exp2, f(settings, '${SHARE_DIR}smth.lua'))

    local exp3 = '/home/user/.cache/app/smth.lua'
    assert.same(exp3, f(settings, '${CACHE_DIR}smth.lua'))
  end)

  it("mk_abspath", function()
    local prev = os.getenv
    ---@diagnostic disable-next-line
    os.getenv = function(name)
      return '/home/user' -- both for PWD and HOME
    end
    assert.same('config', M.mk_abspath('config'))
    assert.same('/home/user/config', M.mk_abspath('~/config'))
    assert.same('/home/user/config', M.mk_abspath('./config'))
    -- restore
    os.getenv = prev
  end)

  it("load_subconf success", function()
    local settings = {
      CONF_DIR = resdir,
      subconfs = nil,
      config = {
        dev_conf = '${CONF_DIR}/table.lua.1'
      }
    }
    local section = 'dev'
    local res, _ = M.load_subconf(settings, section)

    local exp = { key = 'abc', list = { 1, 2, 3 } } -- table from given file
    assert.same(exp, res)
    assert.is_table(settings.subconfs)
    assert.equal(res, settings.subconfs[section])
  end)

  it("load_subconf errors no defined file", function()
    local settings = {
      CONF_DIR = resdir,
      subconfs = nil,
      config = { dev_conf = '${CONF_DIR}/not-exists-file' }
    }
    local section = 'dev'
    local res, err = M.load_subconf(settings, section)

    assert.same(false, res)
    local exp = 'not found file: "./spec/resources/not-exists-file" ' ..
        'for sub-config: "dev"'
    assert.same(exp, err)
    assert.is_nil(settings.subconfs)
    assert.is_nil(settings.subconfs)
  end)

  it("load_subconf no subconf section in conf - use default path", function()
    local settings = {
      CONF_DIR = resdir,
      subconfs = nil,
      config = {}
    }
    local section = 'dev'
    local res, err = M.load_subconf(settings, section)
    assert.same(false, res)
    local exp = 'not found file: "./spec/resources/dev.lua" for sub-config: "dev"'
    assert.same(exp, err)
  end)
end)

describe("base luv", function()
  it("list_of_files", function()
    local f = M.list_of_files
    local exp = {
      './spec/resources/settings.properties',
      './spec/resources/table.html',
      './spec/resources/table.lua.1',
      './spec/resources/table.lua.2'
    }
    assert.same(exp, f(resdir, '.*'))

    local exp2 = { './spec/resources/table.html' }
    assert.same(exp2, f(resdir, '.*html$'))
  end)

  it("pass_date_filter", function()
    local f = M.pass_date_filter0
    -- pass_date_filter0
    local filter = {
      birthtime = { sec_min = 1667456000, sec_max = 1667457000 },
      mtime = { sec_min = 1708862680, sec_max = 1708862690 },
    }
    local stat = { --uv.fs_stat('/usr')
      birthtime = { nsec = 873085176, sec = 1667456759 },
      atime = { nsec = 118454012, sec = 1736003695 },
      ctime = { nsec = 998999324, sec = 1708862689 },
      mtime = { nsec = 998999324, sec = 1708862689 },
      type = "directory",
      -- blksize = 4096, size = 4096, dev = 2050, blocks = 8, flags = 0, gen = 0,
      -- gid = 0, ino = 20, mode = 16877, nlink = 15, rdev = 0, uid = 0,
    }
    assert.same(true, f(filter, stat, 170888000))
  end)

  -- usage example  of list_of_files_recur + data_filter
  it("list_of_files_recur", function()
    if 0 == 0 then return end -- ignore
    local f = M.list_of_files_recur
    local opts = {
      date_filter = { birthtime = { sec_min = 0, sec_max = os.time() } }
    }
    local exp = {}
    assert.same(exp, f('/tmp', nil, nil, opts))
  end)
end)


describe("base properties", function()
  it("load_properties", function()
    local path = M.join_path(resdir, 'settings.properties')
    local exp = {
      {
        key1 = 'value1',
        k = 'value2',
        ['org.some.key2'] = 'value3',
        ['key.b=value4'] = 'value5',
        ['key.c=value6'] = 'value7'
      }
    }
    assert.same(exp, { M.load_properties(path) })
  end)
end)

describe("base substiture", function()
  it("substitute_has_placeholder", function()
    local f = M.substitute_has_placeholder
    assert.same(false, f('${}'))
    assert.same(true, f('${a}'))
    assert.same(true, f('${the_key}'))
    assert.same(true, f('${key}'))
    assert.same(true, f('${-}')) -- corner case do not check key
  end)

  it("substitute_build_kv_map", function()
    local map = {
      cwd = 'something', dir = '${cwd}/subdir', bad = '${unknown}'
    }
    local exp = {
      cwd = 'something', dir = 'something/subdir', bad = '${unknown}'
    }
    assert.same(exp, M.substitute_build_kv_map(map))
  end)

  it("substitute", function()
    local f = M.substitute
    assert.same('KEY', f('${key}', { key = 'KEY' }))
    assert.same('KEY=V', f('${key}=${value}', { key = 'KEY', value = 'V' }))
    --
    assert.same('A=88', f('${k}=${v}', { k = 'A', v = '${V}', V = '88' }))
  end)

  it("substitute_list", function()
    local f = M.substitute_list
    local subs = { k = 'A', v = '${V}', V = '88', b = 'KB', Z_z = '42' }
    local list = { 'simple', '${k}', '${k}=${v}', '9${b}=z', '${Z_z}' }

    local exp = { 'simple', 'A', 'A=88', '9KB=z', '42' }
    assert.same(exp, f(list, subs))
  end)
end)

--[[
todo via stub
describe("base manual", function()
  it("pick_one_item_interactively", function()
    local items = { 'one', 'two', 'free' }
    local i = M.pick_one_item_interactively(items, nil, nil, false)
    assert.same("one", i)
  end)
end)
]]
