install:
	sudo luarocks make

APP_NAME := "cli-app-base"
VERSION := "0.8.4-1"

upload:
	luarocks upload $(APP_NAME)-$(VERSION).rockspec

