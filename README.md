## cli-app-base

Minimalistic library for creating cli applications based on cmd4lua.
Provides base functions and built-in commands(logger+config)

## Installation

```sh
luarocks install cli-app-base
```

```sh
git clone --depth 1 https://gitlab.com/lua_rocks/cli-app-base
cd cli-app-base
luarocks make
```



### Usage Example:


main.lua:
```lua
local handler = require('app.handler')
local settings = require('app.settings')

--
--
local function main(arg)
  settings.setup()
  return handler.handle(arg) -- exit code
end

os.exit(main(arg))
```



configuration file:

app/settings.lua
```lua
local log = require 'alogger'

local base = require 'cli-app-base'
local builtin = require 'cli-app-base.builtin-cmds'

local M = {}

M.appname = 'my-cli-tool'
M.config_file = nil
M.config = nil

local CONF_DIR = os.getenv('HOME') .. '/.config/' .. M.appname .. '/'
local SHARE_DIR = os.getenv('HOME') .. '/.local/share/' .. M.appname .. '/'

local default_config = {
  editor = 'nvim',
  logger = {
    save = true,
    level = log.levels.INFO,
    appname = M.appname
  }
}

function M.setup()
  local apply_env_vars = function() end
  M.config, M.config_file = base.setup(
    default_config, CONF_DIR, SHARE_DIR, apply_env_vars
  )
  -- bind self to buildint commands such as logger, config
  builtin.settings = M
end

return M
```


(cmds-router)
app.handler:
```lua
local Cmd4Lua = require('cmd4lua')

local builtin = require "cli-app-base.builtin-cmds"
local domain = require "app.cmd.domain"

local M = {}
M._VERSION = 'app v1.0.0'

function M.handle(args)
  return
      Cmd4Lua.of(args)
      :root('app')
      :about(tostring(M._VERSION))
      :handlers(M)

      :desc('version of this application')
      :cmd('version', 'v')

      --  the builtin commands

      :desc('built-in commands of the appliction, such as config, logger')
      :cmd('builtin', 'b', builtin.handle)

      --  the domain commands:

      :cmd('domain', 'd', domain.handle)

      :desc('the first command')
      :cmd('first-cmd', 'fc')

      :run()
      :exitcode()
end

function M.cmd_version()
  print(M._VERSION)
end

function M.cmd_first_cmd(w)
  -- .. do something
end

return M
```
