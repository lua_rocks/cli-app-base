-- 24-01-2024 @author Swarg
--
-- basis for creating cli applications
-- Dependencies
--   -  Optional
--      list_of_files: luv

local log = require('alogger')
local uv_ok, uv = pcall(require, "luv")

local M = {}
M.VERSION = 'cli-app-base v0.9.0'
M._URL = 'https://gitlab.com/lua_rocks/cli-app-base'

M.DATE_FORMAT = '%Y-%m-%d-%H-%M-%S'
M.SUBSTITUTE_PATTERN = "%${([%w_]+)}" -- ${KEY} ${THE_KEY}
M.warning_on_bad_conf_syntax = true

local ok_zlib, zlib = pcall(require, 'zlib') -- optional
local BUF_SZ = 4096
local E, v2s, fmt, match = {}, tostring, string.format, string.match


function M.is_os_windows()
  local cwd = os.getenv('PWD')
  -- paths in win has \\ instedad of /
  if not cwd or string.find(cwd, '\\') then
    return true -- win
  end
  return false  -- unix
end

M.is_win = M.is_os_windows()
M.path_sep = M.is_win and '\\' or '/'


---@param vn string
---@param def string
function M.env_or(vn, def)
  local val = os.getenv(vn)
  return (val ~= nil and val ~= '') and val or def
end

--
-- 'a b' -> "a b"
--
---@param arg string
---@return string
function M.wrap_quoted(arg)
  if string.find(arg, ' ') then
    local q = "'"
    if string.find(arg, q) then
      q = '"'
      if string.find(arg, '"') then
        arg = string.gsub(arg, '"', '\\"')
      end
    end
    arg = q .. arg .. q
  end
  return arg
end

--
-- {'ab c', 'd'}  -->  '"ab c" d'
--
---@param cmd string|table
function M.join_args(cmd)
  if type(cmd) == 'table' then
    local s = ''
    for _, arg in pairs(cmd) do
      -- todo smart wrap
      -- join cmd-list to string with wrapping the args in quotes
      s = s .. M.wrap_quoted(arg) .. ' '
    end
    cmd = s
  end
  return cmd
end

-- Extract dir-path from full-file-name
-- '/tmp/dir/file' -> '/tmp/dir/'
-- '/tmp/dir/' -> '/tmp/dir/'
-- 'text' -> nil
---@param path string
function M.extract_path(path)
  if type(path) == "string" then
    return path:match("(.*[/\\])") -- or path
  end
end

function M.extract_extension(path)
  if path then
    local ext = string.match(path, "^.*%.(.+)$") or '';
    if ext and string.find(ext, M.path_sep) then ext = '' end
    return ext
  end
  return ''
end

function M.basename(path)
  if type(path) == "string" then
    return path:match("[^\\/]+$") or path
  end
end

-- /tmp/file.ext --> /tmp/file
---@param fn string
function M.filename_without_extension(fn)
  -- local fn = path:match("[^\\/]+$")
  if fn then
    return fn:match("(.*)[%.]") or fn
  end
end

---@param s string?
function M.is_filename(s)
  return type(s) == 'string' and s ~= '' and s ~= '/' and s ~= '.' and
      s:sub(-1, -1) ~= '/' and s:sub(-1, -1) ~= '.' and
      s:find('*', 1, true) == nil and s:find('..', 1, true) == nil and
      s:find(';', 1, true) == nil
end

---@return string
function M.join_path(...)
  local sep = M.path_sep
  local result = ''
  for i = 1, select('#', ...) do
    local v = select(i, ...)
    if v then
      if type(v) ~= 'string' then
        error('String Expected has[' .. i .. ']: ' .. type(v) .. tostring(v))
      end
      if #result > 0 then
        local has_left = result:sub(-1, -1) == sep
        local has_right = v:sub(1, 1) == sep
        if not has_left and not has_right then
          result = result .. sep
        elseif has_left and has_right then
          result = result:sub(1, -2)
        end
      end
      result = result .. v
    end
  end
  return result
end

-- return parent dir path
-- /path/to/subdir -> /path/to/
-- /path/to/subdir/ -> /path/to/
---@param path string
---@return string?
--
function M.get_parent_dirpath(path, sep)
  sep = sep or M.path_sep

  if type(path) == "string" then
    while true do
      local last = path:sub(-1, -1)
      if last == sep then -- '/' or last == '\\' then
        path = path:sub(1, -2)
      else
        break
      end
    end
    return path:match("(.*[%" .. sep .. "])")
  end
end

-- simple implementation for movin from current working dir by given path
--
-- "/home/dev/project/" , "./api      -> /home/dev/project/api
-- "/home/dev/pro/",      "../../api" -> /home/api
--
-- check relative only from begining of the path.
-- /dir/a/b/c/../..  -  this won't work correctly:
--
function M.build_path(cwd, path, sep)
  sep = sep or M.path_sep

  if cwd and path then
    -- / no use cwd - defined absolute path
    if path:sub(1, 1) == sep then
      return path

      -- ./  relative path from cwd
    elseif path:sub(1, 2) == '.' .. sep then
      path = path:sub(3)
      --
    elseif path == '..' or path == '..' .. sep then -- up
      return M.get_parent_dirpath(cwd, sep) or sep
      --
    else
      -- checks ../  -- relative moving from cwd
      while true do
        local s = path:sub(1, 3)
        if s == '..' .. sep then
          path = path:sub(4)                   -- remove ../ from start
          cwd = M.get_parent_dirpath(cwd, sep) -- dir up
          if not cwd then
            cwd = '/'
            break
          end
        else
          break
        end
      end
    end

    path = M.join_path(cwd, path)
    if path and path:find(sep .. '.' .. sep) then
      path = string.gsub(path, sep .. '%.' .. sep, sep)
    end
    return path
  end
  return cwd
end

---@param dir string?
---@return string
function M.ensure_dirname(dir)
  assert(type(dir) == 'string', 'dir')
  if dir == '' then
    dir = '.' .. M.path_sep -- current directory
  elseif dir ~= '' and dir:sub(-1, -1) ~= M.path_sep then
    dir = dir .. M.path_sep
  end
  return dir
end

--
-- to test is file exist with sudo == true used cmd `sudo test -f ${FILENAME}`
--
---@param filename string?
---@param sudo boolean?
---@return boolean
function M.file_exists(filename, sudo)
  local exists = false
  if filename and filename ~= '' then
    if sudo then
      local code = os.execute('sudo test -f ' .. filename)
      exists = code == 0 or code == true
    else
      local f = io.open(filename, "rb")
      if f then f:close() end
      exists = f ~= nil
    end
  end
  log.debug("file sudo:%s '%s' exists: %s ", sudo ~= nil, filename, exists)
  return exists
end

--
-- check is given string is a path to already existed file
-- for Cmd4Lua
-- usage example:
-- w:desc('...'):pop(base.validate_file_exists):arg()
--
---@param fn string|nil
function M.validate_file_exists(fn)
  if M.file_exists(fn) then
    return true, ''
  end
  return false, 'File Not Exists ' .. tostring(fn)
end

--
-- to test is dir exist with sudo == true used cmd `sudo test -d ${NAME}`
--
---@param name string
---@param sudo boolean?
function M.dir_exists(name, sudo)
  local exists = false
  if name and name ~= '' then
    if sudo then
      local code = os.execute('sudo test -d ' .. name) -- unix
      exists = code == 0 or code == true
    else
      -- the system allows you to open a directory for reading, but
      -- does not allow you to read from it
      local f = io.open(name .. '/', "rb")
      if f then f:close() end
      exists = f ~= nil
    end
  end
  log.debug("dir sudo:%s '%s' exists: %s ", sudo ~= nil, name, exists)
  return exists
end

--
-- check is all given subdirs are exists in given dir
--
---@param dir string
---@param subdirs table
---@param whatis string? for error output
---@return boolean
---@return string? errmsg
function M.is_all_subdirs_exists(dir, subdirs, whatis)
  local err = ''
  if not M.dir_exists(dir) then
    whatis = whatis or ''
    return false, v2s(whatis) .. ' directory not exists: ' .. v2s(dir)
  end
  for _, subdir in ipairs(subdirs) do
    local path = M.join_path(dir, subdir)
    if not M.dir_exists(path) then
      if #err > 0 then err = err .. "\n" end
      err = err .. "directory not exists: " .. v2s(path)
    end
  end
  if #err > 0 then
    return false, err
  end
  return #subdirs > 0, nil
end

--
-- read lines from given file
---@param fn string
---@return table?
function M.read_lines(fn)
  if M.file_exists(fn) then
    local lines = {}
    for line in io.lines(fn) do
      lines[#lines + 1] = line
    end
    return lines
  end
end

function M.has_gz_ext(fn)
  return type(fn) == 'string' and fn:sub(-3) == '.gz'
end

-- if file ends with .gz then unzip it before read
function M.read_lines_ex(fn)
  if fn and M.has_gz_ext(fn) then
    local bin = M.read_all(fn)
    if bin then
      local inflated --[[, eof, bytes_in, bytes_out]] = zlib.inflate()(bin)
      return M.str_split(inflated or '', "\n")
    end
    return nil -- err
  end
  return M.read_lines(fn)
end

--
-- Read all content into one string
--
---@param fn string
---@return string?
function M.read_all(fn)
  if not fn or fn == '' then return nil end

  local file = io.open(fn, "rb")   -- r read mode and b binary mode
  if file then
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
  end
end

---@param fn string
---@param n number? number of lines to read from begin of the given file
---@return table?
---@return number
function M.read_first_lines(fn, n)
  if not fn or fn == '' then return nil, 0 end
  n = tonumber(n) or 1

  local file = io.open(fn, "rb") -- r read mode and b binary mode
  if file then
    local t, i = {}, 0
    while i < n do
      i = i + 1
      local line = file:read "*l"
      if not line then
        i = i - 1
        break
      end
      t[#t + 1] = line
    end
    file:close()
    return t, i
  end
  return nil, 0
end

-- Get all content of a file as one string, or nil it file not exist
---@param path string
---@return string? all content from a file
function M.read_content(path)
  if M.file_exists(path) then
    local f = assert(io.open(path, "rb"))
    local content = f:read("*a")
    f:close()
    return content
  end
  return nil
end

--
-- for lua tables only
-- return '{...}'
-- ^^add
---@param filename string
---@param appdir string?  - prefix if filename is a relative path to file
function M.load_lua_table(filename, appdir)
  if appdir and filename:sub(1, 1) ~= '/' then
    filename = M.join_path(appdir, filename)
  end
  local s = M.read_content(filename)
  if s and s ~= '' then
    if s:sub(1, 1) == '{' then s = 'return ' .. s end
    local func = loadstring(s)
    if func then
      return func()
    end
  end
  return nil
end

-- parse simple text key value into lua table
---@return table
function M.parse_text_to_lua_table(lines)
  local res = {}
  if lines and #lines > 0 then
    for _, line in pairs(lines) do
      local s = line:sub(1, 2);
      if s ~= '# ' and s ~= '//' and s ~= '--' then
        local f = line:gmatch('%S+')
        local key, value = f(), f()
        if key and value then
          res[key] = value
        end
      end
    end
  end
  return res
end

--
-- save given luatable to file via inspect
--
---@param tbl table
---@param filename string
---@param appdir string?
function M.save_lua_table(tbl, filename, appdir)
  if type(tbl) ~= 'table' or type(filename) ~= 'string' then
    return false
  end
  if appdir and filename:sub(1, 1) ~= '/' then
    filename = M.join_path(appdir, filename)
  end
  local str = require("inspect").inspect(tbl)
  return M.write(filename, str, 'wb')
end

--
-- write content to file
--
---@param fn string
---@param content string
---@param mode string? 'w' or 'a'
---@param dry_run boolean?
function M.write(fn, content, mode, dry_run)
  if fn and content then
    mode = mode or 'w'
    if dry_run then
      io.stdout:write("[DRY_RUN] write to '" .. fn .. "':\n")
      io.stdout:write(content .. "\n")
      return true
    end
    local h, err = io.open(fn, mode)
    if h then
      h:write(content)
      h:close()
      return true
    end
    return false, err
  end
  return false
end

--
-- open given file in the external editor
-- write errors to stderr
--
---@param config table?
---@param filename? string
---@param prefix string? used to display what file does not exist
---@param sudo boolean?
---@param search string? goto inside editor after open file
function M.open_file(config, filename, prefix, sudo, search)
  if not filename or filename == '' then
    io.stderr:write("ERROR: empty filename\n")
    return
  end
  if not M.file_exists(filename, sudo) then
    if prefix and prefix ~= '' then prefix = tostring(prefix) .. ' ' end
    prefix = prefix or ''

    io.stderr:write(prefix .. ' File Not Exist: ' .. tostring(filename) .. "\n")
    return
  end

  local editor = (config or {}).editor or 'vim'
  local opts = ''
  -- jumt to search text after file open
  if search and search ~= '' then
    if editor == 'nvim' or editor == 'vim' then
      opts = ' +/' .. tostring(search)
    else
      -- ... todo to other editors
    end
  end

  local cmd = editor .. ' ' .. filename .. opts

  if sudo then cmd = 'sudo ' .. cmd end

  io.stdout:write(tostring(cmd) .. "\n")

  return os.execute(cmd) == 0
end

-- redirect stderr to stdout in the call to popen like this:
-- handle = io.popen("somecommand 2>&1")

--
-- workaround to save file content as root with
-- manually sudo password entering (if needed)
--
-- to write file as root
--
-- right way is:
--
-- sudo sh << SUDO_CMD_EOF
--   cat > filename << END_OF_FILE
-- some multiline
-- content to write into a file
-- END_OF_FILE
-- SUDO_CMD_EOF
--
-- echo 'command for run with a sudo rights' | sudo sh
--
-- sudo cat > fn << INTPUT  -- acts only as user not as a root
--
-- sudo sh << CMD_EOF
--   ls -hal /root/ > /root/test.out
-- CMD_EOF
--
---@param fn string
---@param content string
---@param append boolean? defualt is write(replace old content)
function M.sudo_write(fn, content, append)
  if string.find(content, "\nEND_OF_FILE") then
    error('cannot save content with "END_OF_FILE"')
  end
  if string.find(content, "\nSUDO_CMD_EOF") then
    error('cannot save content with "SUDO_CMD_EOF"')
  end
  -- note mark must begin with a \n(newline sep)

  local redirect = append == true and '>>' or '>'
  local cmd = "sudo sh << SUDO_CMD_EOF\n" ..
      'cat ' .. redirect .. ' ' .. fn .. " << END_OF_FILE\n" ..
      content ..
      "\nEND_OF_FILE" ..
      "\nSUDO_CMD_EOF"

  -- to show why they ask to enter the root password (like a tee)
  io.stdout:write(cmd .. "\n")

  local fh, err = io.popen(cmd)

  if fh then
    -- pcall to avoid "interrupted!" + stacktrace of Ctrl-C
    local ok, output = pcall(fh.read, fh, '*a')
    fh:close()
    if ok then
      return true, output -- success
      --
    elseif output ~= 'interrupted!' then
      return false, output -- throw error? todo trace from pcall
    end

    return ok ~= nil, output
  else
    return false, err
  end
end

--------------------------------------------------------------------------------
--                  Interact with System programs
--------------------------------------------------------------------------------

--
-- run external os command and return its output
---@param cmd string
---@param dry_run? boolean
---@param verbose boolean?
---@return string? Output of command
function M.execr(cmd, dry_run, verbose)
  if dry_run then
    return '[DRY_RUN] ' .. tostring(cmd)
  elseif verbose then
    io.stdout:write(cmd .. "\n")
  end
  log.debug('%s', cmd)

  local fh = io.popen(cmd)
  if fh then
    -- pcall to avoid "interrupted!" + stacktrace of Ctrl-C
    local ok, data = pcall(fh.read, fh, '*a')
    fh:close()
    if ok then
      return data
    elseif data ~= 'interrupted!' then
      error(data) -- todo trace from pcall
    end
  end
end

---@param cmd string
---@param t table?
---@param dry_run boolean?
---@param verbose boolean?
---@return table?
function M.execrl(cmd, t, dry_run, verbose)
  if dry_run then
    t = t or {}
    t[#t + 1] = '[DRY_RUN] ' .. tostring(cmd)
    return t
  elseif verbose then
    io.stdout:write(cmd .. "\n")
  end
  log.debug('%s', cmd)

  local fh = io.popen(cmd)
  if fh then
    t = t or {}
    while true do
      local line = fh:read('*l')
      if not line then break end
      t[#t + 1] = line
    end
    fh:close()
  end
  return t
end

--
-- Execute system cmd and return true on exitcode == 0
--
---@param cmd string
---@param dry_run boolean?
---@param verbose boolean?
---@return boolean
function M.exec(cmd, dry_run, verbose)
  log.debug("exec cmd:'%s', dryrun:%s, verbose:%s", cmd, dry_run, verbose)
  assert(type(cmd) == 'string' and cmd ~= '', 'cmd')
  -- local cmd = 'sudo ' .. editor .. ' ' .. tostring(conf_fn)
  if dry_run then
    io.stdout:write('[DRY_RUN] ' .. cmd .. "\n")
    return true -- ?
  end

  if verbose or cmd:sub(1, 4) == 'sudo' then
    io.stdout:write(cmd .. "\n")
  end

  local code = os.execute(cmd)
  return code == 0 or code == true
end

-- Get a real path from a symbolic link
---@param link string
function M.get_real_path(link)
  if link and link ~= '' then
    local file = io.popen('readlink -f ' .. tostring(link), 'r')
    if file then
      local output = file:read('*all')
      file:close()
      return output
    end
  end
end

-- Epoch TimeStamp to readable DateTime
-- @ param ts number?
---@param f number? (factor - to convert timestamp in ms not sec)
---@return string|osdate
--function M.timestamp2readable(ts, f)
function M.epoch2date(ts, f)
  if ts and type(ts) == 'number' then
    f = f or 1
    return os.date("%H:%M:%S %d-%m-%Y ", ts / f)
  end
  return '?'
end

--------------------------------------------------------------------------------

--
---@param message string?
---@param default string?
function M.build_msg_to_ask_value(message, default)
  return tostring(message) .. ' [' .. tostring(default or '') .. ']: '
end

--
-- ask user for input in interactive mode
--
---@param message string?
---@param default string?
---@param quiet boolean?
---@return string?
function M.ask_value(message, default, quiet, nodef)
  log.debug("ask_value q:", quiet)
  if quiet then return default end

  if message and message ~= nil then
    local prompt = message
    if nodef ~= true then
      M.build_msg_to_ask_value(message, default)
    end
    io.stdout:write(prompt)
    io.stdout:flush()
  end
  local ok, line = pcall(io.stdin.read, io.stdin, "*l")
  if not ok then
    io.stdout:write(tostring(line) .. '\n')
    return nil -- interrupted!
  elseif ok and line == '' then
    line = default
  end
  return line
end

--
-- ask for confirmation (Yes) from user in interactive mode(from cli input)
---@param message string?
---@return boolean
---@param quiet boolean?
function M.ask_confirmation(message, quiet)
  if quiet then return false end -- ?

  io.stdout:write((message or 'Confirm action') .. ' [Yes/No]? ')
  io.stdout:flush()

  local ok, line = pcall(io.stdin.read, io.stdin, "*l")
  if not ok then
    io.stdout:write(tostring(line) .. '\n')
  elseif ok and (line == 'y' or line == 'Y' or line == 'yes') then
    return true
  end
  return false
end

---@param items table (list not a map)
---@param fmt_fn function?
---@param si number?
---@param ei number?
function M.fmt_items(items, fmt_fn, si, ei)
  local fmt0 = fmt_fn or string.format
  local s = ''
  si = si or 1
  ei = ei or #(items or {})
  for i = si, ei do
    local item = items[i]
    s = s .. fmt0("%3s  %s\n", i, item)
  end
  return s
end

---@param message string?
---@param items table
---@param fmt_fn function?
---@param quiet boolean?
---@return boolean
---@return any
function M.pick_item_interactively(items, message, fmt_fn, quiet)
  if quiet == true then return false, nil end
  local s = M.fmt_items(items, fmt_fn, nil, nil)
  s = s .. (message or 'Select One Item') .. ': '

  io.stdout:write(s)
  io.stdout:flush()

  local ok, line = pcall(io.stdin.read, io.stdin, "*l")
  if not ok then
    io.stdout:write(tostring(line) .. '\n')
    return false, nil
  end
  if not line or line == '' or line == 'q' then
    return false, nil
  end
  local idx = tonumber(line)
  -- todo multiple items for given rage or indexes like 1-10 1,4,8 and so on
  local item = items[idx or false]

  return item ~= nil, item
end

--
-- read line from io.stdin if read_line
-- Wait for input (supported multiline commands with \\ at end):
--
---@param message string?
---@param default string?
---@param quiet boolean? if yes skip read and return defualt value
function M.read_line(message, default, quiet)
  if not quiet then
    if message then
      io.stdout:write(message .. "\n")
    end
    local res = ''
    repeat
      local ok, line = pcall(io.stdin.read, io.stdin, "*l")
      if ok then
        res = res .. line
      end
      if res:sub(-1, -1) == '\\' then -- multiline command
        res = res:sub(1, -2)
      else
        break -- one line command
      end
    until not ok or not line

    return res
  end

  return default
end

--------------------------------------------------------------------------------
--                            Table Utils
--------------------------------------------------------------------------------

--
--
---@param t table
---@param elm any
function M.tbl_index_of(t, elm)
  assert(type(t) == 'table', 't')

  for index, value in ipairs(t) do
    if value == elm then
      return index
    end
  end
  return -1
end

-- remove element from given index and return it
-- if index out of range then return nil
---@param t table -- array
---@param i number -- index
function M.tbl_pop_elm(t, i)
  assert(type(t) == 'table', 't')
  assert(type(i) == 'number', 'idx')
  local sz = #t
  if i == sz then
    local el = t[i]
    t[sz] = nil
    return el
    --
  elseif i > 0 and i < sz then
    local el = t[i]
    t[i] = t[sz] -- swap
    t[sz] = nil
    return el
  end
  return nil -- not removed
end

-- Create full-deep-copy-of table
---@private
---@param tbl table
---@return table new one
function M.tbl_deep_copy(tbl)
  local ret = {}
  if type(tbl) == 'table' then
    for k, v in pairs(tbl) do
      if type(v) == 'table' then
        ret[k] = M.tbl_deep_copy(v)
      elseif v ~= nil then
        ret[k] = v
      end
    end
  end
  return ret
end

-- Check is tbl a not empty table list with number indexes
---@private
function M.tbl_islist(tbl)
  if type(tbl) == 'table' then
    -- or table is list not map
    -- Tests if a Lua table can be treated as an array.
    local count = 0
    for k, _ in pairs(tbl) do
      if type(k) == 'number' then -- check is all 'keys' are numner indexes
        count = count + 1
      else
        return false
      end
    end
    return (count > 0)
  end
  return false
end

---@param t table
---@return table
function M.tbl_keys(t)
  assert(type(t) == 'table', 'table expected')
  local kt = {}
  for k, _ in pairs(t) do
    kt[#kt + 1] = k
  end

  return kt
end

function M.tbl_key_count(t)
  assert(type(t) == 'table', 'table expected')
  local c = 0
  for _, _ in pairs(t) do c = c + 1 end
  return c
end

--
---@param t table
---@param sort_func function?
function M.tbl_sorted_keys(t, sort_func)
  assert(type(t) == 'table', 'table expected')
  local kt = {}
  for k, _ in pairs(t) do
    kt[#kt + 1] = k
  end

  table.sort(kt, sort_func)

  return kt
end

-- show keys names and types
---@param t table
---@return string
function M.tbl_flat_inspect(t)
  local s = ''
  if type(t) == 'table' then
    for k, v in pairs(t) do
      s = s .. v2s(k) .. ': ' .. type(v) .. "\n"
    end
  end
  return s
end

---@param map table
---@param ftype string
---@return table key names for given ftype
---@return table -- all other key names do not match the required type
function M.tbl_keys_filter_by_type(map, ftype)
  local t, other = {}, {}
  for k, v in pairs(map) do
    if type(v) == ftype then
      t[#t + 1] = k
    else
      other[#other + 1] = k
    end
  end
  return t, other
end

---@param keys table
---@param max number?
---@return number
function M.tbl_max_str_key_len(keys, max)
  max = max or 0
  for _, key in ipairs(keys) do
    if type(key) == 'string' and #key > max then
      max = #key
    end
  end
  return max
end

function M.tbl_list_to_map(list, value)
  value = value == nil and true or value
  local m = {}
  for _, key in ipairs(list) do
    m[key] = value
  end
  return m
end

--
-- {key=123} -> {{"key", 123}}
--
---@param map table
function M.tbl_map_to_list(map)
  local t = {}
  for k, v in pairs(map) do
    t[#t + 1] = { k, v }
  end
  return t
end

---@param t1 table
---@param t2 table
---@return table
function M.tbl_join_lists_to_new(t1, t2)
  local t = {}
  for _, e in ipairs(t1) do t[#t + 1] = e end
  for _, e in ipairs(t2) do t[#t + 1] = e end
  return t
end

--
-- merge two given tables(maps) to new one table
-- keys from map2 will be override keys from map1
-- can be used to define options by default map(map1) and given by user (map2)
--
---@param map1 table
---@param map2 table
---@return table
function M.tbl_merge_maps_to_new(map1, map2)
  local t = {}
  for k, v in pairs(map1) do t[k] = v end
  for k, v in pairs(map2) do t[k] = v end
  return t
end

--
-- append elements from src to dst from istart to iend positions
--
---@param dst table
---@param src table
---@param istart number?
---@param iend number?
function M.tbl_add_list(dst, src, istart, iend)
  istart = istart or 1
  iend = iend or #src
  local step = istart <= iend and 1 or -1

  for i = istart, iend, step do
    dst[#dst + 1] = src[i]
  end

  return dst
end

---@param t1 table
---@param t2 table
function M.tbl_compare_map_by_keys(t1, t2)
  local no_t1, no_t2 = {}, {}
  for k1, _ in pairs(t1) do
    if t2[k1] == nil then
      no_t2[#no_t2 + 1] = k1
    end
  end
  for k2, _ in pairs(t2) do
    if t1[k2] == nil then
      no_t1[#no_t1 + 1] = k2
    end
  end
  return no_t1, no_t2
end

---@param a table
---@param b table
---@return boolean
function M.tbl_compare_map_by_second_num_elm_desc(a, b)
  local acnt = tonumber(a[2]) or 0
  local bcnt = tonumber(b[2]) or 0
  return acnt > bcnt
end

--
-- open given "path" (of keys) in luatable
--
---@param tbl table
---@param spath string|table
---@return boolean, any
function M.tbl_get_node(tbl, spath, sep)
  sep = sep or '.'
  local path
  if type(spath) == 'string' then
    path = M.str_split(spath, sep)
  elseif type(spath) == 'table' then
    path = spath
  else
    error('expected table or string got: ' .. type(spath))
  end

  log.debug('path:', path)
  local t, i = tbl, 0

  while t do
    i = i + 1
    if i > #path then
      break
    end
    local key = path[i]
    log.debug('i:%s: key:%s', i, key) -- M.tbl_sorted_keys(t))

    if type(t) ~= 'table' then
      return false, fmt('in depth:%s(%s) got:%s', v2s(i), v2s(key), type(t))
    end
    t = t[key]
    log.debug('i:%s key:%s node:%s', i, key, type(t))
    if t == nil then
      return false, fmt('no key at depth:%s(%s)', v2s(i), v2s(key))
    end
    -- if type(t) ~= 'table' and
  end

  return t ~= nil, t
end

--
--
--
---@param opts table?{raw}
function M.tbl_format_as_list(t, key_order, opts)
  opts = opts or E
  local inspect = opts.inspect or require "inspect"

  if opts.raw or (type(key_order) ~= 'table' or #key_order == 0) then
    return inspect(t)
  end

  local sorted_keys = M.tbl_sorted_keys(t)
  local max_key_len = opts.max_key_len or M.tbl_max_str_key_len(sorted_keys, 0)

  local s = ''
  local passed_keys = {}
  local ignore_keys = opts.ignore_keys or {}
  local fmtptrn = "%" .. v2s(max_key_len) .. "s: %s\n"

  local function add_pair(k)
    local v = t[k]
    if v ~= nil and not ignore_keys[k] then
      if type(v) == 'table' then
        v = inspect(v)
      end
      s = s .. fmt(fmtptrn, v2s(k), v2s(v))
    end
  end

  for _, k in ipairs(key_order) do
    add_pair(k)
    passed_keys[k] = 1
  end

  for _, k in ipairs(sorted_keys) do
    if not passed_keys[k] then
      add_pair(k)
    end
  end

  return s
end

--------------------------------------------------------------------------------

---@param s string
---@param sep string?
---@param out table?
function M.str_split(s, sep, out)
  assert(type(s) == 'string', 'string expected')
  assert(not out or type(out) == 'table', 'output table')
  assert(not sep or type(sep) == 'string', 'string sep')
  out = out or {}

  if sep == nil or sep == ' ' then
    sep = "%s"
  end

  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    out[#out + 1] = str
  end
  return out
end

---@param s string
---@return string
function M.str_first_word(s)
  return string.match(s or '', "^%s*([^%s]+)") or ''
end

function M.str_trim(s)
  return string.match(v2s(s), '^%s*(.-)%s*$')
  -- return (s:gsub("^%s*(.-)%s*$", "%1"))
end

-- remove empty lines "..   \n" -> "..\n"
function M.str_replace_trailing_whitespace(s)
  return type(s) == 'string' and string.gsub(s, "% +\n", "") or s
end

function M.str_regex_escape(str)
  return str:gsub("[%(%)%.%%%+%-%*%?%[%^%$%]]", "%%%1")
end

---@param str string
---@param start string ends
function M.str_starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

---@param str string?
---@param e string ends
function M.str_ends_with(str, e)
  return str ~= nil and e ~= nil and (e == "" or str:sub(- #e) == e)
end

--
-- count the number of occurrences of a substring
--
---@param str string?
---@param sub string?
---@param off number? default is 1 offset position to start
---@param plain boolean? default is true(not a pattern just string)
function M.str_substr_count(str, sub, off, plain)
  if str and sub and #str >= #sub then
    plain = plain == nil and true or plain
    off = off or 1
    local c = 0
    local p = off
    while p <= #str do
      local _, j = string.find(str, sub, p, plain)
      if not j then break end
      c = c + 1
      p = j + 1
    end
    return c
  end
  return 0
end

function M.str_is_true(s)
  return s ~= nil and (s == 'true' or s == 't' or s == '1' or s == 1 or s == '+')
end

--
-- 1024 -> 1.0Kb
--
---@param n number
function M.numsize_to_readable(n)
  if type(n) == 'number' then
    local f = 1024
    -- if n < f then
    --   return tostring(n)
    if n >= f * f * f then
      return fmt('%0.1fTb', n / (f * f * f))
    elseif n >= f * f then
      return fmt('%0.1fMb', n / (f * f))
    else --if n >= f then
      return fmt('%0.1fKb', n / f)
    end
  end
  return tostring(n)
end

-- 1000 -> 1.0K
---@param n number?
function M.numcnt_to_readable(n)
  if type(n) == 'number' then
    local f = 1000
    if n < f then
      return tostring(n)
    elseif n >= f * f * f then
      return fmt('%0.1fT', n / (f * f * f))
    elseif n >= f * f then
      return fmt('%0.1fM', n / (f * f))
    elseif n >= f then
      return fmt('%0.1fK', n / f)
    end
  end
  return tostring(n)
end

--
-- for table formating
--
---@param title string
---@param sizes table{str:string=v:number}
---@param min_str string?
function M.str_align_center(title, sizes, min_str)
  assert(type(sizes) == 'table', 'c')
  local maxlen = sizes[title or false]
  if type(maxlen) ~= 'number' then
    error('unkwnon maxlen for ' .. v2s(title))
  end

  if min_str and #title > maxlen then
    title = min_str
  end

  local diff = maxlen - #title
  if diff > 1 then
    local pad = string.rep(' ', diff / 2)
    title = pad .. title .. pad
    if #title < maxlen then
      title = title .. string.rep(' ', maxlen - #title) -- center
    end
  elseif diff < 0 then
    sizes[title] = #title
  end

  return title
end

---@param maxs table
---@param key string
---@param value string
function M.str_check_maxlen(maxs, key, value)
  local maxlen = maxs[key] or 0
  if value and #value > maxlen then
    maxs[key] = #value
  end
  return maxs
end

---@param cwd string
---@param path string
function M.get_relative_path(cwd, path)
  if #(cwd or '') < #(path or '') then
    cwd = M.ensure_dirname(cwd)
    if M.str_starts_with(path, cwd) then
      return path:sub(#cwd + 1)
    end
  end
  return path
end

--------------------------------------------------------------------------------
--                   App Settings and Configuration
--------------------------------------------------------------------------------

--
-- the minimal valid config contains a logger field
--
---@param config any
---@return boolean
function M.is_valid_config(config)
  if type(config) == 'table' then
    return type(config.logger) == 'table'
  end
  return false
end

-- to show messages into stderr before setup alogger config
local function verbose(f, fmsg, ...)
  if f and type(fmsg) == 'string' then
    io.stderr:write(log.format(fmsg, ...) .. "\n")
  end
end

--
-- Load settings from the config file
--
---@param default_config table
---@param conf_dir string
---@param share_dir string
---@param apply_env_vars function?  - callback
---@param v boolean? verbose
function M.setup(default_config, conf_dir, share_dir, apply_env_vars, v)
  assert(type(default_config) == 'table', 'default_config')
  local config_file = (conf_dir .. 'config.lua')
  local config

  -- load from file
  local has_conf = M.file_exists(config_file)
  verbose(v, "config dir:%s file:%s exists:%s", conf_dir, config_file, has_conf)

  if has_conf then
    verbose(v, 'load config-file: ', config_file)
    local loaded = M.load_lua_table(config_file)
    if M.is_valid_config(loaded) then
      config = loaded
    else
      verbose(v or 'WARN', '[WARN] Cannot parse the existed config-file: %s %s',
        config_file, 'Check the syntax of the lua table')
    end
  end

  -- if not loaded - use defaults
  if not config then
    verbose(v, 'Config could not be loaded. Generating default config...')
    config = M.tbl_deep_copy(default_config)
  end

  local lconf = config.logger or {}
  lconf.log_dir = lconf.log_dir or M.join_path(share_dir, 'logs')
  lconf.log_file = lconf.log_file or M.join_path(lconf.log_dir, 'latest.log')
  lconf.app_root = (_G.APP or {}).appdir or '.'
  -- override the lazy fs saving setup
  lconf.force_setup = lconf.force_setup ~= nil and lconf.force_setup or true
  config.logger = log.setup(lconf) -- return a copy of an original table
  config.logger.force_setup = nil

  if type(apply_env_vars) == 'function' then -- custom logic for a specific app
    apply_env_vars(config)
  end

  return config, config_file
end

-- configure logger to output into stdout not a file
---@param appname string
---@param dir string
---@param print function?
function M.mk_testing_logger_conf(appname, dir, print)
  dir = dir or '/tmp/'
  return {
    save = false,
    level = log.levels.DEBUG,
    silent_debug = false,
    appname = appname or 'app',
    log_dir = M.join_path(dir, "logs"),
    log_file = M.join_path(dir, "latest.log"),
    print = print,
  }
end

--
-- fix issue with attempt to save props from the logger config (print-func)
--
---@return string
---@param config table
---@param tosave boolean? hide config.logger.{print,app_root,appname}
function M.config_to_string(config, tosave)
  assert(type(config) == 'table', 'config')
  local oprint, oapp_root, oappname

  if tosave then -- do not save to the config file
    local l = config.logger
    oprint, oapp_root, oappname = l.print, l.app_root, l.appname
    l.print, l.app_root, l.appname = nil, nil, nil
  end

  local str = require("inspect").inspect(config)

  -- roll back
  if tosave then
    local l = config.logger
    l.print, l.app_root, l.appname = oprint, oapp_root, oappname
  end

  return str
end

--
-- create directory if needed
-- if dir already exists return true
-- returns fals then cannot create a new dir
--
---@param dir string
---@param sudo boolean?
function M.mkdir(dir, sudo)
  local prefix = sudo ~= nil and 'sudo ' or ''

  if not M.dir_exists(dir) then
    return os.execute(prefix .. 'mkdir -p ' .. dir) == 0
  end
  return true
end

---@param fn string
---@param config table
---@param dry_run boolean?
function M.save_config(fn, config, dry_run)
  local conf = M.config_to_string(config, true)
  local confdir = M.extract_path(fn)

  if M.mkdir(confdir) and M.write(fn, conf, "wb", dry_run) then
    return true
  end

  return false
end

-- ${CONF_DIR} -> to actual path
---@param settings table
---@param fn string
---@return string
function M.normalize_path(settings, fn)
  assert(type(settings) == 'table', 'settings')
  assert(type(fn) == 'string', 'fn')
  local path = string.gsub(fn, M.SUBSTITUTE_PATTERN, settings):gsub('//', '/')
  return path
end

-- ~/somedir -> /home/user/shomedir
function M.mk_abspath(path)
  if path then
    local fc2 = path:sub(1, 2)
    if fc2 == '~/' then
      path = (os.getenv('HOME') or '/home/user/') .. path:sub(2)
    elseif fc2 == './' then
      path = M.join_path(os.getenv('PWD'), path:sub(2))
    end
  end
  return path
end

function M.get_subconf_path(settings, section)
  local key = tostring(section) .. '_conf'
  local path = ((settings or E).config or {})[key]
  if not path then
    log.debug('no settings.config.%s use default path', key)
    path = M.join_path(settings.CONF_DIR, section .. '.lua')
  end
  return M.normalize_path(settings, path)
end

---@param settings table
---@param section string
---@param conf table
local function set_subconf(settings, section, conf)
  assert(type(settings) == 'table', 'setting')
  assert(type(section) == 'string', 'section')
  settings.subconfs = settings.subconfs or {}
  settings.subconfs[section] = conf
  return conf
end

---@param section string
---@param settings table
function M.get_subconf(settings, section)
  return (settings.subconfs or E)[section or false]
end

---@param settings table
---@param section string
---@param def_conf table?
function M.new_subconf(settings, section, def_conf, overwrite)
  local path, errmsg = M.get_subconf_path(settings, section)
  if not path then return false, errmsg end
  def_conf = def_conf or {}
  if not overwrite and M.file_exists(path) then
    return false, 'cannot overwrite alredy existed file ' .. path
  end

  if not M.save_lua_table(def_conf, path) then
    return false, 'cannot save new conf to ' .. path
  end

  local conf = set_subconf(settings, section, def_conf)
  return conf, path
end

---@param section string
---@return table|false, string?
function M.load_subconf(settings, section)
  local path = M.get_subconf_path(settings, section)
  if not M.file_exists(path) then
    return false, fmt('not found file: "%s" for sub-config: "%s"', path, section)
  end

  local conf = M.load_lua_table(path)
  if type(conf) ~= 'table' then
    return false, fmt('cannot parse config file "%s"', path)
  end

  set_subconf(settings, section, conf)
  return conf, path
end

---@return boolean, string
function M.save_subconf(settings, section)
  local path = M.get_subconf_path(settings, section)
  local conf = M.get_subconf(settings, section)

  if type(conf) ~= 'table' then
    return false, 'not found subconf "' .. v2s(section) .. '"'
  elseif not M.save_lua_table(conf, path) then
    return false, fmt('cannot save config file "%s"', path)
  end

  return true, path
end

--
--
---@param dstfn string
---@param srcfn string
---@param can_rewrite boolean?
function M.copy_file(srcfn, dstfn, can_rewrite)
  if not M.file_exists(srcfn) then
    return false, 'not found source file: ' .. tostring(srcfn)
  end
  if not can_rewrite and M.file_exists(dstfn) then
    return false, 'cannot rewrire already existed file: ' .. tostring(dstfn)
  end

  local f1, errmsg1 = io.open(srcfn, "rb")
  if not f1 then
    return false, errmsg1
  end
  local f2, errmsg2 = io.open(dstfn, "wb")
  if not f2 then
    f1:close()
    return false, errmsg2
  end

  -- while true do
  local all = f1:read("*a")
  -- if not all then break end
  if all then
    f2:write(all)
  end
  -- end

  f1:close()
  f2:close()
  return true
end

--
--
-- copy a file using luasocket  (suitable for copying large binary files)
--
-- (rewrite already existed)
--
-- sudo luarocks install luasocket  # for ltn12
--
function M.copy_file_ex(path_src, path_dst)
  local ok, ltn12 = pcall(require, 'ltn12')
  if not ok then
    return false, 'not found ltn12 from luasocket: ' .. v2s(ltn12)
  end

  -- aliases for protected environments
  local io_open = io.open
  local hsrc, err_r = io_open(path_src, "rb")
  if not hsrc then
    return false, err_r
  end

  local hdst, err_w = io_open(path_dst, "wb")
  if not hdst then
    hsrc:close()
    return false, err_w
  end

  local ok2, err = ltn12.pump.all(ltn12.source.file(hsrc), ltn12.sink.file(hdst))
  -- handlers was closed by ltn12 -- hsrc:close() hdst:close()

  return ok2 == 1, err -- nil
end

--------------------------------------------------------------------------------

--
-- Check that the specified input file exists and that the output file can be
-- rewritten (either does not exist yet or the force_rewrite flag is set)
-- If the output file already exists or not defined, find the next available
-- filename for it by adding a number suffix to the end of its name or fni
--
-- path/to/file.ex -> path/to/file.ex.1 -> path/to/file/.ex.2
--
---@param fni string  - filename of the input
---@param fno string? - filename of the output
---@param force_rewrite boolean?
---@return boolean
---@return string -- selected output filename
function M.validate_input_n_output_filenames(fni, fno, force_rewrite)
  if type(fni) ~= 'string' then
    return false, 'expected string input-filename got: ' .. tostring(fni)
  end
  if fno ~= nil and type(fno) ~= 'string' then
    return false, 'expected string output-filename or nil got: ' .. tostring(fno)
  end
  if not M.file_exists(fni) then
    return false, 'file not found ' .. tostring(fni)
  end
  if fno == fni then
    return false, 'Cannot read and write to the same file at the same time'
  end

  -- select next free filename to output
  if not fno then
    local n = 0
    local prev_n = string.match(fni, '%.(%d+)$')
    if prev_n and tonumber(prev_n) then
      n = tonumber(prev_n) or n
      fni = fni:sub(1, #fni - (#prev_n + 1))
    end
    while true do
      n = n + 1
      fno = fni .. '.' .. n
      if force_rewrite or not M.file_exists(fno) then
        break
      end
    end
  end

  if type(fno) ~= 'string' then
    return false, 'Cannot select filename for output'
  end
  if not force_rewrite and M.file_exists(fno) then
    return false, 'Cannot rewrite already existed file ' .. tostring(fno)
  end

  return true, fno
end

--------------------------------------------------------------------------------
--                          Testing Utils
--------------------------------------------------------------------------------

-- build the function to get
---@param resource_dir string
---@param throw_error_on_nofile boolean?
---@return function
function M.build_fn_get_resource(resource_dir, throw_error_on_nofile)
  assert(type(resource_dir) == 'string', 'resource_dir')

  local dir = resource_dir
  local error_on_nofile = throw_error_on_nofile

  return function(fn)
    local path = M.join_path(dir, fn)
    if not M.file_exists(path) then
      if error_on_nofile == nil or error_on_nofile then
        error('Not Found file "' .. tostring(path) .. '"')
      end
      return nil
    end
    return M.read_all(path)
  end
end

function M.build_fn_get_resource_path(resource_dir)
  assert(type(resource_dir) == 'string', 'resource_dir')

  local dir = resource_dir

  return function(...)
    return M.join_path(dir, ...)
  end
end

--
-- print message (and varargs) to stdout only when f == true
-- Note: will not add a newline to the end of the string
--       adds spaces between params(...) but skip empty strings
--
-- Usage Example:
--   vprint(verbose, '.')
--   vprint(verbose, 'message', '', 'b', "\n")           ->  "message b\n"
--   vprint(verbose, 'key_a:', 16, 'key_b:', 18, "\n")
--
---@param f boolean?
---@param msg string
function M.vprint(f, msg, ...)
  if f and msg then
    local s = tostring(msg)
    for i = 1, select('#', ...) do
      local str = select(i, ...)
      if str ~= '' then
        s = s .. ' ' .. tostring(str)
      end
    end
    io.stdout:write(s)
    io.stdout:flush()
    return s
  end
end

--
-- debuggin
--
-- Save given data only if envvar is exists and not empty
--
-- By design, envvar should have an application-specific name, for example
-- "MY_APP_DUMP_RESP" whose value is expected to be the path where to save the
-- given data, and then, if specified, print a message to stdout in the form
-- where some details are saved.
-- Formatting from "alogger" with all its advanced features is used to format
-- the message (fmsg + ...)
function M.save_dump_on(envvar, data, fmsg, ...)
  if type(envvar) == 'string' then
    local path = os.getenv(envvar) -- 'TET_SAVE_RESP')
    if type(path) == 'string' and path ~= '' then
      if path == '.' then path = './' .. envvar .. '.dump' end
      if type(data) ~= 'string' then
        data = log.inspect0(data)
      end
      M.write(path, data, 'wb')
      if type(fmsg) == 'string' then
        print(path, log.format(fmsg, ...))
      end
    end
  end
end

--------------------------------------------------------------------------------



--
-- to use this you should has the "luv"-rock installed into your system
--
-- aternative execrl('-ls ' .. dir)
--
---@param dir string
---@param filter string|nil for string.match
---@return table|false, string?
---@param opts table?{ftype}
function M.list_of_files(dir, filter, opts)
  assert(uv_ok, 'the luv library must be installed')

  local ftype = (opts or E).ftype
  local basenames = (opts or E).basenames
  assert(ftype == nil or ftype == 'file' or ftype == 'directory', 'ftype')

  local handle, errmsg = uv.fs_scandir(dir)
  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end
  local t = {}

  if dir:sub(-1, -1) ~= M.path_sep then dir = dir .. M.path_sep end

  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    if not ftype or ftype == typ then -- filter by file|directory
      if not filter or string.match(name, filter) then
        if basenames then
          t[#t + 1] = name
        else
          t[#t + 1] = dir .. name
        end
      end
    end
  end
  return t
end

--
-- build flat list of all filenames(recursive) in given directory
-- without empty directories
--
-- todo: protection against symlinks deadlocks
--
---@return table
---@param opts table?{ignored_dirs}
function M.list_of_files_recur(dir, filter, out, opts)
  assert(uv_ok, 'the luv library must be installed')

  local handle = uv.fs_scandir(dir)
  if type(handle) == "string" or not handle then return out end

  out = out or {}
  local path_sep = M.path_sep
  local ignored_directories = (opts or E).ignored_directories or {}
  local dfilter = (opts or E).date_filter
  local has_date_filter = type(dfilter) == 'table'
  local now
  if has_date_filter and opts then
    opts.now = opts.now or os.time()
    now = opts.now
  end

  if dir:sub(-1, -1) ~= path_sep then dir = dir .. path_sep end
  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    if typ == 'directory' and not ignored_directories[name] then
      local abs_path = dir .. name
      if not has_date_filter or M.pass_date_filter(dfilter, abs_path, now) then
        M.list_of_files_recur(abs_path, filter, out, opts)
      end
      --
    elseif typ == 'file' then
      if not filter or match(name, filter) then
        local abs_path = dir .. name
        if not has_date_filter or M.pass_date_filter(dfilter, abs_path, now) then
          out[#out + 1] = abs_path
        end
      end
    end
  end

  return out
end

--
--  Note: in linux you can get epoch time via `stat -c%Y filename`
--
---@param filter table{mtime, btime}
---@param now number
function M.pass_date_filter(filter, path, now)
  assert(type(filter) == 'table', 'filter')
  return M.pass_date_filter0(filter, uv.fs_stat(path), now)
end

--
-- check whether the output uv.fs_stat (file data) will pass given filter,
-- the specified filter compares the brithtime and mtime(modiffication)
--
-- can be used to search and filter files by their creation(modification) time
--
---@param filter table{mtime, btime}
---@param stat table{mtime, btime}
---@param now number
function M.pass_date_filter0(filter, stat, now)
  assert(type(filter) == 'table', 'filter')
  assert(type(stat) == 'table', 'stat') -- from uv.fs_stat

  if stat and stat.birthtime and stat.mtime then
    if filter.btime then
      local min = assert(filter.btime.sec_min, 'btime.sec_min') -- or 0
      local max = filter.btime.sec_max or now                   -- os.time()
      local btime = assert(stat.birthtime.sec, 'birthtime.sec')
      if btime < min or btime > max then
        return false
      end
    end
    if filter.mtime then
      local min = assert(filter.mtime.sec_min, 'btime.sec_min')
      local max = filter.mtime.sec_max or now -- os.time()
      local mtime = assert(stat.mtime.sec, 'mtime')
      if mtime < min or mtime > max then
        return false
      end
    end
    return true
  end

  return false
end

function M.parse_time()
  -- 2024-12-01 03:48:19.572003789 +0300
end

--
-- decompress gz file via zlib:
-- if tofn nil or equal fn then remove original compressed file after
-- decompressing.(decompress into temp file and remove it to original)
--
-- if you just need to unzip but not delete the original, just specify `tofn`
--
---@param fn string path to file
---@param tofn string?
---@return boolean, number|string -- ok, size-or-error
function M.gzip_decompress_file(fn, tofn)
  log.debug('ungzip file', fn)
  assert(ok_zlib and type(zlib) == 'table', zlib)

  local src, errmsg = io.open(fn)
  if not src then
    return false, 'cannot read file ' .. v2s(fn) .. ' ' .. v2s(errmsg)
  end
  local fn_tmp, keep
  if tofn == nil or tofn == fn then
    fn_tmp, keep = (fn .. '.ungzip'), false
  else
    fn_tmp, keep = tofn, true
  end

  local dst, err = io.open(fn_tmp, 'wb') --io.tmpfile()
  if not dst then
    src:close()
    return false, 'cannot create new tmp file ' .. v2s(err)
  end

  local inflate = zlib.inflate()
  local shift = 0
  local total_bytes = 0

  while true do
    local data = src:read(BUF_SZ)
    if not data then break end
    local inflated, eos, bytes_in, bytes_out = inflate(data)
    if type(bytes_out) == 'number' then
      total_bytes = total_bytes + bytes_out
    end
    dst:write(inflated)
    if eos then
      src:seek("set", shift + bytes_in)
      shift = shift + bytes_in
      inflate = zlib.inflate()
    end
  end

  src:close()
  if not keep then
    dst:close()
    os.remove(fn)
    os.rename(fn_tmp, fn)
  end

  return true, total_bytes
end

-- 01-08-2024 @author Swarg
--


--
-- to load properties config file into flat luatable
-- (copy from db_env/conf/properties)
--
---@param path string
---@param mapping table? to map short key to full-key
---@return table?
---@return string? errmsg
function M.load_properties(path, mapping)
  assert(type(path) == 'string', 'path')

  local with_mapping = type(mapping) == 'table' ---@cast mapping table

  local file, err = io.open(path, "rb") -- no error raised when file not exists
  if not file then
    return nil, err
  end

  local t = {}

  while true do
    local line = file:read("*l")
    if not line then
      break
    end
    local key, value = string.match(line, "^%s*([^#]+)=(.*)$")
    if key and value then
      if (with_mapping and mapping[key]) then
        key = mapping[key] -- shortkey
      end
      t[key] = value
    end
  end

  file:close()

  return t, nil
end

------
--
--  Substitutions ${KEY} ${KEY}
--

function M.substitute_has_placeholder(s)
  return s ~= nil and #s > 3 and string.find(s, '${', 1, true) ~= nil
end

function M.substitute_build_kv_map(substitutions)
  local kv_map = {}
  for k, value in pairs(substitutions) do
    value = tostring(value)
    if M.substitute_has_placeholder(value) then
      value = string.gsub(value, M.SUBSTITUTE_PATTERN, substitutions)
    end
    kv_map[k] = value
  end
  return kv_map
end

--
-- build string from template based substitutions
--
-- in template keys must be specified in this manear:
-- "${KEY}=..." in the "substitutions" table  keys must be without "${" and "}"
-- See SUBSTITUTE_PATTERN
--
---@param template string string with placeholders to be substitured
---@param substitutions table - map with key-value pairs
---@param kv_map table?
---@return string
function M.substitute(template, substitutions, kv_map)
  assert(type(template) == 'string', 'expected string template')
  assert(type(substitutions) == 'table', 'expected table(map) substitutions')

  kv_map = kv_map or M.substitute_build_kv_map(substitutions)

  local s = string.gsub(template, M.SUBSTITUTE_PATTERN, kv_map)

  return s
end

---@param list table
---@param substitutions table?
---@param kv_map table?
function M.substitute_list(list, substitutions, kv_map)
  -- ${dir} = ${cwd}/subsid -> get cwd to
  kv_map = kv_map or M.substitute_build_kv_map(substitutions)
  local t = {}
  for i = 1, #list do
    t[#t + 1] = M.substitute(list[i], E, kv_map)
  end
  return t
end

return M
