-- 11-06-2024 @author Swarg
-- shared utils to work with http: send (HEAD|GET|POST)requests, decode data
--
-- @Deprecated  TODO rewrite projects to use "lhttp" luarock-package
--

local log = require 'alogger'

local zlib = require 'zlib' -- for gzip  luarocks lua-zlib
local cjson = require 'cjson'
local http = require 'socket.http'
local ltn12 = require 'ltn12'

local base = require 'cli-app-base'

local M = {}

local E, v2s, fmt = {}, tostring, string.format

M.APP_JSON = 'application/json'
M.FORM_DATA = 'application/x-www-form-urlencoded'
M.ACCEPT_ALL = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8'
M.TEXT_XML = 'text/xml'
-- 'text/xml; charset=UTF-8'


---@param code any
---@param status any
---@return string
---@param headers table?
local function mk_con_error(code, status, headers)
  local s = fmt('code: %s %s', v2s(code), v2s(status))
  if code == 301 and type(headers) == 'table' then
    s = ' location: ' .. v2s(headers['location'])
  end
  return s
end

--
-- about given url based on HEAD request
--
---@param url string
---@param headers table?
---@param out table?
function M.send_head(url, headers, out)
  log.debug("send_head")
  assert(type(url) == 'string', 'url')
  local resp = {}
  local req = {
    scheme = "https",
    method = 'HEAD',
    url = url,
    headers = headers or {},
    sink = ltn12.sink.table(resp)
  }
  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then
    return false, mk_con_error(code, status, h)
  end

  return true, {
    size = tonumber(h['content-length']),
    ctype = h['content-type'],
    etag = h.etag,
    cache_control = h['cache-control']
  }
end

---@param url string
---@param def string?
local function get_scheme(url, def)
  return url ~= nil and string.match(url, '^(%w+)://') or def
end

--
-- download GET responce for given url
--
---@param url string
---@param headers_or_accept table|string|nil
---@return boolean, string?
function M.send_get(url, headers_or_accept, out)
  log.debug("send_get", url, headers_or_accept)
  assert(type(url) == 'string', 'url')
  local headers, expected_content

  if type(headers_or_accept) == 'string' then
    headers = {
      accept = headers_or_accept, -- expected_content or M.APP_JSON,
      ['accept-encoding'] = "gzip",
    }
    expected_content = headers.accept
  elseif type(headers_or_accept) == 'table' then
    headers = headers_or_accept
  end

  local response_body = {}
  local req = {
    scheme = get_scheme(url, "https"),
    method = 'GET',
    url = url,
    headers = headers,
    sink = ltn12.sink.table(response_body),
  }
  local has_out = type(out) == 'table'
  if has_out then
    out.req = req
  end

  local r, code, resp_headers, status = http.request(req)
  if has_out then
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then
    return false, mk_con_error(code, status, h)
  end

  -- parse json response
  local content_type = h['content-type']
  if expected_content then
    if not content_type or not string.find(content_type, expected_content, 1, true) then
      return false, fmt('unexpected content-type: "%s" expected:"%s"',
        v2s(content_type), v2s(expected_content))
    end
  end

  local respbody = table.concat(response_body)

  if h["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(respbody)
    respbody = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp body len:", #respbody)

  return true, respbody
end

--
-- send POST request (json)
-- initially used only for transmitting json data
--
---@param url string
---@param post_body string (json)
---@return boolean, string?
-- send_post_json
function M.send_post(url, post_body, accept, out)
  log.debug("send_post")
  assert(type(url) == 'string', 'url')

  local response_body = {}

  local payload = v2s(post_body)
  local source = ltn12.source.string(payload);
  local payload_len = #payload

  local headers = {
    accept = accept or M.APP_JSON,
    ['content-type'] = M.APP_JSON,
    ['content-length'] = payload_len,
  }
  if payload_len > 1024 then
    headers['accept-encoding'] = "gzip"
  end

  local req = {
    scheme = get_scheme(url, "https"),
    method = 'POST',
    url = url,
    headers = headers,
    sink = ltn12.sink.table(response_body),
    source = source
  }
  log.debug('POST', req.url)
  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then
    -- TODO PUTCH DELETE UPDATE etc
    return false, mk_con_error(code, status, h)
  end

  -- parse json response
  local content_type = h['content-type']
  if not content_type or accept and not string.find(content_type, accept) then
    log.debug('Not Supported Content-Type:%s expected:%s ', content_type, accept)
    return false
  end

  local resp_body = table.concat(response_body)

  if h["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(resp_body)
    resp_body = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp-body len:", #resp_body)

  return true, resp_body
end

--------------------------------------------------------------------------------

---@param url string
---@param dst string
---@param headers table?
function M.download_file(url, dst, headers, out)
  log.debug("download_file", url, dst, headers)
  assert(type(url) == 'string', 'url')

  -- local response_body = {}
  local req = {
    scheme = "https",
    method = 'GET',
    url = url,
    headers = headers or {
      -- accept = 'application/json',
      ['accept-encoding'] = "gzip",
    },
    sink = ltn12.sink.file(io.open(dst, 'wb'))
  }
  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then -- todo 204 partial
    return false, mk_con_error(code, status, h)
  end

  local total_bytes = 0

  if h["content-encoding"] == "gzip" then
    local ok, sz = base.gzip_decompress_file(dst)
    if not ok then
      return false, sz -- errmsg
    end
    if ok and type(sz) == 'number' then
      total_bytes = sz
    end
  else
    local cont_len = tonumber(h['content-length'])
    if cont_len then
      total_bytes = cont_len
    end
  end

  return true, total_bytes
end

--------------------------------------------------------------------------------

--
-- decode json from string to luatable
--
---@param str_json string
function M.decode_json(str_json)
  assert(type(str_json) == 'string', 'raw_json')
  local ok, decoded = pcall(cjson.decode, str_json)
  if not ok then
    local err_msg = decoded
    log.debug('Cannot parse json responce: %s', err_msg)
    return nil
  end
  return decoded -- table
end

--------------------------------------------------------------------------------
--                           HTML Parser
--------------------------------------------------------------------------------

--
-- a simplified way to get the body range(substring) of a given html-tag
-- If an 'attr' is specified and 'tag_open' is not closed by a ">" symbol, then
-- find and return the range of only those tags whose attributes include the
-- specified substring 'attr'
--
-- by default, if the tag and attribute are not specified, searches for the
-- body of the first script-element in the given html-document
--
---@param html string
---@param tag_open string?   -- <script ...  or <script>
---@param tag_close string?  -- </script>
---@param attr string?       -- optional used if tag_open NOT ends with '>'
---@param off number?        -- start position in the html to find
---@param range_end number?  -- end position  to limit the search area)
---@return number, number
function M.get_tag_body_range(html, tag_open, tag_close, attr, off, range_end)
  log.debug("get_tag_body_range html:%s open:%s close:%s offset:%s",
    html ~= nil, tag_open, tag_close, off)
  if html then
    -- range
    off = off or 1                 -- start
    range_end = range_end or #html -- end

    -- tag to find
    tag_open = tag_open or '<script'
    tag_close = tag_close or '</script>'

    local closed_opend_tag = tag_open:sub(-1, -1) == '>'
    local found = false
    local pstart
    local has_attr = type(attr) == 'string' and attr ~= '' and attr ~= ' '

    -- by design, in order to also search by attribute inside the opening tag,
    -- the opening tag must not end with the '>' symbol
    if closed_opend_tag and has_attr then
      log.debug('Cannot find closed tag_open:"%s" for attr:"%s" ', tag_open, attr)
      return -1, -1
    end

    while true do
      pstart = string.find(html, tag_open, off, true)
      if not pstart or pstart > range_end then
        log.debug('Not Found content with tags "%s","%s"', tag_open, tag_close)
        return -1, -1
      end
      pstart = pstart + #tag_open

      if not closed_opend_tag then -- open tag is not closed - can check attr
        local i = string.find(html, '>', pstart, true)
        if not i or i > range_end then
          log.debug('Cannot find end of the tag:"%s" end:%s', tag_open, range_end)
          return -1, -1
        end
        found = true

        if attr and attr ~= '' then
          local attr_offset = string.find(html:sub(pstart, i), attr, 1, true)
          if not attr_offset then
            found = false
            off = i + 1
          else
            pstart = i + 1
            break
          end
        end
        pstart = i + 1
        --
      elseif attr and attr ~= '' then
        -- case then cannot check attr between <tag ... & >
        log.debug('Got an open_tag with ">" and attr', tag_open)
        found = false
        return -1, -1
      end

      if not attr or closed_opend_tag then
        break
      end
      -- try to find next given pair tag + attr
    end

    if not found then
      log.debug('Not Found tag:"%s" with attr:"%s"', tag_open, attr)
      return -1, -1
    end

    local pend = string.find(html, tag_close, pstart, true)
    if not pend or pend > range_end then
      log.debug('Not Found the tag_close:"%s" from pos: %s', tag_close, pstart)
      return -1, -1
    end

    return pstart, (pend - 1)
  end
  return -1, -1
end

---@param htmlbody string
---@param otag string
---@param ctag string
---@param attr string?
---@param pstart number?
---@param pend number?
---@return table
function M.find_all_tags(htmlbody, otag, ctag, attr, pstart, pend)
  assert(type(htmlbody) == 'string', 'htmlbody')
  local res = {}
  local off = pstart or 1
  pend = pend or #htmlbody

  while true do
    local s, e = M.get_tag_body_range(htmlbody, otag, ctag, attr, off, pend)
    if s < 0 or e < 0 then
      break
    end
    res[#res + 1] = { s, e } -- start and end position
    off = e + 1
  end

  return res
end

--
-- create a readable report of found occurrences of tags
--
---@param html string
---@param positions table ranges
---@param opts table?
function M.get_readable_tag_ocurrences(html, positions, opts)
  assert(type(html) == 'string', 's')
  assert(type(positions) == 'table', 't')
  local verbose = (opts or E).verbose
  -- todo show parent element, or xpath to root

  local s = ''
  for _, t in ipairs(positions) do
    local ps, pe = t[1], t[2]
    s = s .. v2s(ps) .. '-' .. v2s(pe)
    if verbose then
      s = s .. ' ' .. string.sub(html, ps, pe)
    end
    s = s .. "\n"
  end
  if verbose then
    s = s .. 'Total: ' .. #positions
  end

  return s
end

---@return string?, number
function M.get_tag_body(html, tag, pstart, pend)
  local otag, ctag = '<' .. tag, '</' .. tag .. '>'
  local s, e = M.get_tag_body_range(html, otag, ctag, nil, pstart, pend)
  if s > -1 and e > -1 then
    return html:sub(s, e), (e + #ctag)
  end
  return nil, pend
end

---@param html_table string
function M.parse_html_table(html_table, out)
  out = out or {}
  local ps, pe = 1, #html_table
  local range = function(ps0, pe0, tag, attr)
    local otag, ctag = '<' .. tag, '</' .. tag .. '>'
    return M.get_tag_body_range(html_table, otag, ctag, attr, ps0, pe0)
  end

  local s, e = -1, -1
  s, e = range(ps, pe, 'thead')
  if s < 0 or e < 0 then return false, 'not found thead' end

  -- parse thead(columns names)
  s, e = range(ps, pe, 'tr')
  if s < 0 or e < 0 then return false, 'not found tr in thead' end

  local thead, tbl = {}, {}

  while true do
    local body, next = M.get_tag_body(html_table, 'th', s, e)
    if not body or type(next) ~= 'number' then break end
    thead[#thead + 1] = body
    s = next
  end
  local columns = #thead
  if columns == 0 then
    return false, 'empty thead no columns names'
  end

  tbl[1] = thead

  local bs, be = range(ps, pe, 'tbody')
  if bs < 0 or be < 0 then return false, 'not found tbody' end

  while bs < #html_table do
    local trs, tre = range(bs, be, 'tr')
    if trs < 0 or tre < 0 then break end

    local row, col = {}, 0
    while true do
      col = col + 1
      if col > columns then break end

      local body, next = M.get_tag_body(html_table, 'td', trs, tre)
      if not body or type(next) ~= 'number' then break end
      row[thead[col]] = body
      -- TODO sanitaze body - escape quotes
      trs = next
    end

    tbl[#tbl + 1] = row
    bs = tre + 1
  end


  return true, tbl
end

return M
