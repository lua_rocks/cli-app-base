-- 24-01-2024 @author Swarg

local log = require('alogger')
---@diagnostic disable-next-line: unused-local
local cmd4lua = require('cmd4lua')

local base = require("cli-app-base")

local M = {}
-- this field must be filled in by the application itself that uses this module
-- the value of this field is the entire module responsible for setting and
-- configuration. Something like: app.settings, what contains which fields as:
-- CONFIG_DIR, SHARE_DIR, config, config_file, setup, save, validate_config, etc.
M.settings = nil -- ref to app settings (config)

--
--------------------------------------------------------------------------------
--                         COMMANDS INTERFACE
--------------------------------------------------------------------------------

--
---@param w Cmd4Lua
function M.handle(w)
  w:about('Built-in')

      :desc('Work with Config')
      :cmd('config', 'c', M.cmd_config)

      :desc('Configure Logger')
      :cmd('logger', 'l', M.cmd_logger)

      :desc('Raise an error for debugging')
      :cmd('throw-error', 'te', M.cmd_throw_error)

      :run()
end

--
--------------------------------------------------------------------------------
--                      COMMANDS  IMPLEMENTATIONS
--------------------------------------------------------------------------------

--
--------------------------------------------------------------------------------
--                              CONFIG
--------------------------------------------------------------------------------

--
--
---@param w Cmd4Lua
function M.cmd_config(w)
  w
      :about('Builtin config')

      :desc('Validate the entire progress of loading an existing config file')
      :cmd('validate', 'v', M.config_open)

      :desc('Show actual configuration status')
      :cmd('inspect', 'i', M.config_inspect)

      :desc('Show content of the config file')
      :cmd('cat', 'c', M.config_cat)

      :desc('Show path of the config file')
      :cmd('path', 'p', M.config_path)

      :desc('Open config in the editor')
      :cmd('open', 'o', M.config_open)

      :desc('back up the configuration file')
      :cmd('backup', 'b', M.config_backup)

      :desc('rollback the configuration file from backup')
      :cmd('roolback', 'roll', M.config_rollback)

      :run()
end

local function validate_settings()
  if type(M.settings) ~= 'table' then
    error('no settings. check the yourapp.settings.setup() method call')
  end
end



--
-- Validate the entire progress of loading an existing config file
--
---@param w Cmd4Lua
function M.cmd_validate(w)
  if not w:is_input_valid() then return end
  validate_settings()
  error('Not implemented yet')
end

--
-- Show actual configuration status
--
---@param w Cmd4Lua
function M.config_inspect(w)
  local debug_mode = w:desc('set log level to debug'):has_opt('--debug', '-d')
  if not w:is_input_valid() then return end
  validate_settings()
  if debug_mode then log.fast_setup(false, log.levels.DEBUG) end
  io.stdout:write(require 'inspect'.inspect(M.settings.config))
end

--
---@param w Cmd4Lua
function M.config_cat(w)
  if not w:is_input_valid() then return end
  validate_settings()
  if base.file_exists(M.settings.config_file) then
    io.stdout:write(base.read_all(M.settings.config_file))
  else
    io.stderr:write('No such file ' .. tostring(M.settings.config_file) .. "\n")
  end
end

--
---@param w Cmd4Lua
function M.config_path(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end
  validate_settings()

  local res = M.settings.config_file
  if w:is_verbose() then
    local b = base.file_exists(M.settings.config_file)
    res = res .. ': file ' .. (b == true and 'exists' or 'not exists')
  end
  io.stdout:write(res .. "\n")
end

--
-- Open already existed app config file
-- if the file does not exist, offer to create it based on the current config
--
---@param w Cmd4Lua
function M.config_open(w)
  w:v_opt_quiet('-q')
  w:v_opt_dry_run('-d')
  local yes = w:desc('confirm creation of the new config file')
      :has_opt('--yes', '-y')

  if not w:is_input_valid() then return end
  validate_settings()

  local fn = M.settings.config_file

  if not base.file_exists(fn) then
    local msg = 'Create config file "' .. fn .. '"'
    if yes or base.ask_confirmation(msg, w:is_quiet()) then
      if not base.save_config(fn, M.settings.config, w:is_dry_run()) then
        return w:error('cannot create config file: ' .. tostring(fn))
      end
    else
      return w:error('config file does not exists: ' .. tostring(fn))
    end
  end

  base.open_file(M.settings.config, fn, 'Config')
end

--
-- back up the configuration file
--
---@param w Cmd4Lua
function M.config_backup(w)
  w:v_opt_quiet('-q')
  w:v_opt_dry_run('-d')

  local dir = w:desc('directory for backup'):opt('--dst-dir', '-D')
  local can_rewrite = w:desc('can rewrite existed backup file')
      :has_opt('--can-rewrite', '-w')

  if not w:is_input_valid() then return end

  local fn = M.settings.config_file

  if not base.file_exists(fn) then
    return w:error('config file does not exists: ' .. tostring(fn))
  end
  dir = dir or base.join_path(M.settings.SHARE_DIR, 'backups')
  base.mkdir(dir)

  local dst_basename = 'config-' .. os.date(base.DATE_FORMAT) .. '.lua'
  local dst_path = base.join_path(dir, dst_basename)

  local ok, errmsg = base.copy_file(fn, dst_path, can_rewrite)
  print(dst_path, ok, errmsg or '')
end

--
-- rollback the configuration file from backup
--
---@param w Cmd4Lua
function M.config_rollback(w)
  w:v_opt_quiet('-q')
  w:v_opt_dry_run('-d')
  local dir = w:desc('directory for backup'):opt('--dst-dir', '-D')

  if not w:is_input_valid() then return end

  local curr_cfg_fn = M.settings.config_file

  dir = dir or base.join_path(M.settings.SHARE_DIR, 'backups')
  if not base.dir_exists(dir) then
    return w:error('directory not found' .. tostring(dir))
  end
  local files = base.list_of_files(dir)
  if not files or #files == 0 then
    return w:error('no backups files at' .. tostring(dir))
  end

  local msg = 'Select one backup file to rollback'
  local okf, bk_cfg_fn = base.pick_item_interactively(files, msg)
  if not okf or type(bk_cfg_fn) ~= 'string' then
    return w:error('canceled')
  end

  if base.file_exists(curr_cfg_fn) then
    local dst_basename = 'prev_config_before_rollback.lua'
    local dst_path = base.join_path(dir, dst_basename)

    local ok, errmsg = base.copy_file(curr_cfg_fn, dst_path, true)
    if not ok then
      w:error('cannot make backup copy before rollback ' .. tostring(errmsg))
      return
    end
  end

  local ok, errmsg = base.copy_file(bk_cfg_fn, curr_cfg_fn, true)
  print(bk_cfg_fn, ok, errmsg or '', curr_cfg_fn)
end

--
--------------------------------------------------------------------------------
--                              LOGGER
--------------------------------------------------------------------------------

--
---@param w Cmd4Lua
function M.cmd_logger(w)
  if w:desc('Open the current latest log file'):is_cmd('open', 'o') then
    validate_settings()
    base.open_file(M.settings.config, log.get_outfile(), 'Log')
    --
  elseif w:desc('Clear the current latest log file'):is_cmd('clean', 'c') then
    print(log.purge())
    --
  elseif w:desc('Change the logger settings'):is_cmd('set', 's') then
    M.cmd_logger_set(w)
    --
  elseif w:desc('Inspect the log config'):is_cmd('inspect', 'i') then
    print(require 'inspect'.inspect(log.__get_config()))
    --
  elseif w:desc('The current Logger settings'):is_cmd('status', 'st') then
    print(log.status())
    --
  elseif w:desc('Show supported levels'):is_cmd('levels', 'l') then
    print(log.get_supported_levels())
    --
  elseif w:desc('Show the tail of the log file'):is_cmd('tail', 't') then
    M.cmd_logger_tail(w)
    --
  elseif w:desc('Issue the log message with given params')
      :is_cmd('message', 'm') then
    print(M.cmd_logger_issue_message(w))
    --
  end
end

--
-- logger set
--
---@param w Cmd4Lua
function M.cmd_logger_set(w)
  local desc, has_changes

  --
  if w:desc('set saving to file'):is_cmd('save', 's') then
    desc = 'set to save a log messages to the file'
    local saving = w:desc('saving', desc):pop():argb()
    log.set_save(saving)
    print(log.status())
    has_changes = true --?
    --
  elseif w:desc('set the log level '):is_cmd('level', 'l') then
    local alvl = w:desc('the log level'):pop(M.validate_log_level):arg()
    if w:is_input_valid() then
      log.set_level(log.level_str2i(alvl))
      print(log.status())
      -- print(string.format('Logger Now Level is %d:%s ',
      --   log.get_level(), log.get_level_name()))
      has_changes = true
    end
  end

  if w:desc('Save a changes to the config')
      :has_opt('--save-config', '-sc') then
    if has_changes then
      M.config_save()
    else
      print('Nothing changed')
    end
  end
end

--
--
--
function M.validate_log_level(val)
  local lvl = val and log.level_str2i(val) or nil
  if not log.is_valid_lvl(lvl) then
    local msg = 'Expected One of [' .. tostring(log.get_supported_levels()) .. ']'
    return false, msg
  end
  return true
end

--
-- Create a new log message with given log level and text
--
---@param w Cmd4Lua
function M.cmd_logger_issue_message(w)
  local alvl = w:desc('level', 'the log level'):pop(M.validate_log_level):arg()
  -- build a one line from a multiple words
  local msg = w:desc('message', 'the log message')
      :optional():pop():arg()
  local ask_message = w:desc('input message through interactive mode(readline)')
      :has_opt('--read-line', '-r')

  if not w:is_input_valid() then return end
  if ask_message or not msg then
    msg = base.ask_value('Log Message')
  end
  print('>', msg)
  log.log(log.level_str2i(alvl), msg or '')
end

--
--
---@param w Cmd4Lua
function M.cmd_logger_tail(w)
  local n = w:desc():def(8):optn('--lines', '-n')
  local cmd = 'tail'
  if n then cmd = cmd .. ' ' .. '-n ' .. tostring(n) end
  cmd = cmd .. ' ' .. log.get_outfile()
  print(cmd)
  print(base.execr(cmd))
end

--------------------------------------------------------------------------------

--
-- Raise an error for debugging
-- Just show stack-trace to determine where the application is installed
--
---@param w Cmd4Lua
function M.cmd_throw_error(w)
  local msg = w:def('error message'):opt('--message', '-m')

  if not w:is_input_valid() then return end

  error(msg)
end

return M
